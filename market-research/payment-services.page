---
title: Payment Services
categories: market-research
...

In selecting payment services, we are considering:

- Do we like the businesses ethically?
- How FLO are they?
- Do they offer international service?
- How are the fees?
- Any policy concerns? Privacy concerns?
- Hassle for users? Popularity?

Obviously, we want to choose the best overall that we can.

There are two typical systems: If credit-cards are the focus, per-transaction rate is typically 2.9% + 30¢ with some variation. Many traditional processors (which we decided against and do not list here) offer a monthly fee with slightly lower per-transaction rate. The other major category is private payment networks: users deposit funds via ACH/eCheck and fees are much lower.

Other misc. services exist with some variations..

## Services we use now

### Our crowdmatching system

We have integrated with [Stripe](https://stripe.com) which charges the standard 2.9% + 30¢ and will allow us to combine charges to multiple projects (although we haven't implemented that, it will wait until beta when we have multiple projects). We've started by using proprietary JavaScript with the hopes to eventually move to follow CrowdSupply's approach of using their own interface to then process card info and send to Stripe's API without us ever holding any of the data.

### Outside donations

For those giving us donations outside of the crowdfunding system, we have accepted donations in the past from Dwolla, but they no longer offer that sort of direct-donate service. We got funds through Balanced Payments via a Tilt.com campaign, but those services have since shut down. We accept donations through Paypal as well. David set up a BitCoin wallet, but we only have received a few donations that way.

**Money Orders, Certified Checks, Cashier's Checks, Personal Checks** — For those who do not want to accept any of the compromises of online payment sites or who may not have a bank account or credit card, we will accept these types of checks by mail. Checks should be made out to "Snowdrift.coop" and you can [contact us](/p/snowdrift/contact) and let us know how to best reach you to provide mailing address and clarify any questions. *Donations directly to us as a project during our start-up stage are non-refundable.* Once we are operating, we will credit the funds from any such checks to the user's Snowdrift.coop account only after the payments have fully cleared (and withdrawals of funds deposited by check depend on us first clarifying any potential legal concerns).

## Services research

We reviewed many options, but the market is always changing. Below are some we felt were worth noting. Our research remains incomplete at this time.

| Service | Fees | Proprietary Issues | Pros  | Cons |
|-----------|---------|--------------------------|--------|--------|
| [Stripe](https://stripe.com) | 2.9% + 30¢ | possible but difficult to set up without their proprietary JS, overall proprietary other than APIs | robust developer features, international currencies (for 2% fee), can handle split-charges (one system charge split to many recipients) | only for credit-cards |
| [Braintree](https://www.braintreepayments.com) | 2.9% + 30¢ | probably many | may offer just the sorts of services we need | a Paypal company… |
| [PayPal](http://paypal.com) | 2.9% + 30¢ (U.S.), extra for currency conversion, alternate micropayment rate | Lots of proprietary but functions without proprietary JS | Widest use, robust international support (despite fees), ACH and cards (but no discount for ACH) | History of unethical business practices and user complaints |
| [WePay](https://www.wepay.com) | 2.9% + 30¢ (cards) or 1% + 30¢ (bank ACH) | emphasizes their proprietary risk engine, lots of proprietary, investigation needed whether functional without proprietary JS | Focused on transparent experience, dedicated focus on serving crowdfunding platforms etc. | ? |
| [Payza](http://payza.eu) | 2.9% + 30¢ | *investigation needed* probably proprietary mostly | Money Order / Certified Check option as well as credit/debit/bank | has some BTC support, some other complexities |
| [Amazon Payments](https://payments.amazon.com) | 5% + 5¢ under $10 payments, otherwise 2.9% + 30¢ | surely proprietary stuff, exact issues need investigation | Many people already registered | Amazon.com is especially secretive, opaque company, ethical issues |
| [Dwolla](http://dwolla.com) | monthly platform fee? | lots | uses bank-direct
instead of CC, low/no fee other than platform fee? | ? |
| [Square](https://squareup.com) | 2.9% + 30¢ | proprietary system overall | ? | not sure |


### Misc options to research further

There are a bunch of things like [Venmo](https://venmo.com) / [Square Cash](https://cash.app/) and others that do mobile payments without credit cards. These systems are highly suspect as being mainly designed for lock-in along with ad-tracking as their primary business. This trend is nice in getting away from fees and credit cards but otherwise worrisome. What about the new [Current](https://current.com/) is a debit-card for teens…

More stuff (see [wikipedia list](https://en.wikipedia.org/wiki/List_of_online_payment_service_providers)…

* [Uphold](https://uphold.com) is some cryptocurrency thing… 
* The European version of ACH is called [Direct Debit](https://en.wikipedia.org/wiki/Direct_debit) and [GoCardless](https://gocardless.com) is a 1% fee service for that; also, it is supposedly less of a security issue to deal with this directly compared with ACH.
* Bill2phone / other mobile-phone payments?
* [Web Payments](https://web-payments.org/) is a proposed system for W3C standard for money transfers. If that gets realized, we will want to consider it, of course.
* [**Fairbill**](https://fairbill.com) — European-based processor with some range of FLO-sympathies and ethical values, details need investigation
* [MangoPay](http://www.mangopay.com/) some European thing with international support, seems really robust, and there's [yesod-mangopay](https://hackage.haskell.org/package/yesod-mangopay) already…
* [PayGarden](http://www.paygarden.com) is some new scheme to get funds out of corporate gift cards. Probably like a more robust version of selling a gift card to someone who was going to buy the store's products anyway. Not sure about details here, surely there's some fees. This does allow a more anonymous system.
* [Google Pay](https://pay.google.com/about/) — probably not, but look into anyway?
* **[GNU Taler](http://taler.net) — upcoming digital payment system that seems the most optimally matched to our values: anonymity for payers, transparency for payees, currency-neutral, online, efficient, FLO**
* **Bitcoin** — supported now by Stripe and others, plus known services like [Coinbase](https://coinbase.com)
    * Bitcoin is volatile, speculative, Ponzi-ish, and has other issues. The main benefit is that Bitcoin users tend to understand and care more about FLO issues compared to others.
    * In the ever-changing Bitcoin world, we're not sure what services are best for us overall.
    * For wallet management and more, [btcd](https://github.com/conformal/btcd) software is one option to investigate
    * [Blockchain](https://blockchain.info/api) also offers some info about processing BitCoins and API etc.
* [Other cryptocurrencies](http://coinmarketcap.com) — like Bitcoin, we won't focus on these but could engage them such that if people want to send us them, we could learn to exchange them or something, although perhaps there's some other ways to make this work.
* [Stellar](https://stellar.org) is a fork of [Ripple](https://ripple.com), both of which are quirky and complex with a pretense of a distinctly conceptual trust-network separate from their currencies
    * Money as debt-tracking (which is the origin of money anyway): "look, instead of me paying you, my friend owes me for something, so how about you do this thing for me and my friend will just owe you instead?" Ripple takes that extended debt idea and automatically connects huge networks of people who don't know each other enough to extend debt on their own, and when things cycle around, debts can just be canceled. There are many other details to make the system functional, but we'll have to wait and see
    * We could be a Ripple gateway. Funded project team members could opt to take Ripple credit instead of regular withdrawal. That Ripple credit can make its way around the Ripple system until either a real withdrawal is requested to clear the credit (at which point we allow them to withdraw \$ as usual) or someone with a linked Ripple credit chooses to fund their Snowdrift patron account (at which point we can just move the credit and everyone's debts are canceled and there's no extra fees). We will not accept just any Ripple credit, but we will accept credit if it can be linked back to us.
    * some [reporting on Ripple/Stellar](http://observer.com/2015/02/the-race-to-replace-bitcoin/) certainly gives major pause about any of it.
* oink.com — generic commercial type site hmm probably not

### International services

Some services like Paypal work internationally, but some others are specifically dedicated to currency-exchange and international transfers.

* [**Transferwise**](https://transferwise.com) — low-fee currency exchange concept. With their new Borderless accounts, once people do identity verification, they can hold accounts in multiple currencies. There are no fees (right?) within currencies, so we could do small processing that way and the only fees come when there's withdrawals, which are still lower than most other services…
* [Tipalti](http://www.tipalti.com) — some service that takes care of all the legal and financial complications of sending funds to various places
* [XE.com](http://xe.com) and [OFX](https://ofx.com) seem to be good low-fee options for sending funds internationally (we also looked into Moneygram and Western Union, but those are designed more for personal rather than business accounts)
* [Trans-Pay](http://trans-pay.com) — not clear from initial skim, pricing isn't set, is described as adjusted for businesses based on each case
* [Currency Fair](https://www.currencyfair.com/) — another low-cost currency thing to look into eventually
* <https://www.xoom.com> (a Paypal service…)
* <http://www.ria.com/> — some Russian service?
