---
title: The Economics of Public Goods
categories: communications
...

Snowdrift.coop addresses the specific problems facing *public goods*. To understand those problems, we need to explain the basic economic concepts.

## Rivalrous vs non-rivalrous goods

**Scarcity forms the basis for our supply-and-demand market economy.** Two people cannot both eat the same apple. We may not literally *consume* some things, but two people cannot wear the same hat at the same time. Economists use the term "rivalry" to refer the inherent competition for these **rivalrous goods**. Rivalrous goods come in two forms: publicly-owned "common goods" (such as fish in public waterways) and "private goods" (most physical items whether commercial or personal).

**Some resources have no scarcity.** We have effectively limitless air and sunlight.^[However, *access* to things like clean air and sunlight may be rivalrous or scarce in some cases.] Ideas are not rivalrous (although development time and energy is!). Learning, using, and sharing ideas does not consume them. When you give an idea to someone else, you still have it yourself. Snowdrift.coop focuses on these **non-rivalrous goods.**^[Snowdrift.coop as a market of sorts has some parallels to markets for rivalrous goods. Our focus on democratic cooperation also relates to the complex challenges of managing common goods (which simple market mechanisms cannot adequately address). However, we do not work directly with either sort of rivalrous goods.]

<div style="text-align: center;">
![](/assets/nina/Transmission_10fps2.gif)^[This animation comes from Nina Paley's wonderful summary of issues with [cultural transmission in today's world](http://blog.ninapaley.com/2013/12/07/make-art-not-law-2/).]
</div>

### Public goods vs club goods

Economists classify non-rivalrous goods as either public goods or club goods.^[Note that, in this context, "goods" includes intangible examples such as abstract concepts and patterns. A few other terms sometimes work. "Products" emphasizes the production process and the time and energy of people developing things. "Content" puts emphasis on the context such as how a website *contains* text, images, programs, and so on.]

***Public goods* refers to those non-rivalrous resources open to everyone to use and share freely.** *Club goods* are non-rivalrous resources where access and/or other privileges are reserved for members of an exclusive club (such as those who pay a fee).

#### Challenges in funding development of public goods

All club good would actually provide greater value to more people as a public good. But **how can we support the time and energy needed to do the research and development if everyone can freeride and get the results without supporting the development work?** Various [existing mechanisms](existing-mechanisms) provide important support, but most public goods projects remain woefully underfunded.

## A Brief History of "Intellectual Property"

Today, the internet makes sharing ideas and creative work so easy that we immediately recognize the awkward artifice of treating ideas the same way we treat scarce goods. Yet we got to this point through a history in which ideas were more strongly tied to their physical containers.

### Ideas vs physical property

Through most of human existence, ideas were spread almost entirely through stories, songs, and in-person training with no physical container. Ideas were either shared freely and openly or controlled through secrecy and censorship. Either way, nobody confused them with physical goods. The modern concept of "intellectual property" developed as lectures, storytelling, and concerts gave way to physical media containing texts, drawings, and audio/video recordings.

### The first mass medium and first copyright laws

The printing press created the first physical mass medium (books), but we had no modern copyright law until nearly 270 years later.^[[The Statute of Anne](https://en.wikipedia.org/wiki/Statute_of_Anne) was passed in 1710, 270 years after the invention of printing press in 1440. The Statute served to provide a *limited* monopoly of (at-most) 28 years as economic incentive to authors to write works.] Furthermore, for most of its history, copyright law functioned as an industrial regulation affecting the relationships between authors and publishers. Private citizens did not have printing presses and so were not *directly* affected.

#### Changing technology

Unlike print, recorded sound and video have existed for less than a couple human generations, and the technology changed continually and rapidly over that time. Widely-published computer programs are an even newer phenomenon.

**Each technological shift in media has been accompanied by backlash from businesses based on the previous system.** Live radio musicians protested the early playing of recorded discs; movie producers did all they could to try to stop home videotape technology; and so on.

![](/assets/nina/ME_283_KillingScribes-640x199.png)

The physical nature of vinyl records, video cassettes, and digital discs — along with propaganda from publishers — led many people to associate creative works with their physical media.

### Digital technology frees ideas again

Digital technology has essentially freed recorded works from their scarce containers. Copies of works no longer require new physical objects.

### Laws and business models have not adapted

Our laws and cultural assumptions remain largely tied to the 20th century era of mass-produced physical media for creative works.

![](/assets/nina/ME_415_RealityAboveLaw-640x199.png)

Threatened by the upheaval of the shift to digital media, publishing companies have entrenched themselves with a mix of lobbying for legal restrictions on sharing (such as through extensions of copyright law) and the development of Digital Restrictions Management (DRM) technologies.

## Economics of club goods

![](/assets/nina/ME_160_Rivalrous1-640x199.png)

### Three impacts of artificial restrictions

1. **Funding:** Through pay-for-access business models, we can fund creative work.^[and pay-for-access club goods feel familiar and similar to the private-goods market economy, even though supply-and-demand and other free market principles don't actually work the same way.]
2. **Exclusion:** Restrictions limit access, sharing, and creative derivations. Those who can't afford to pay a license fee simply have no access (unless they break the law and crack any technological restrictions). Potential adaptations may never happen, hampering progress.
3. **Power:** With control over resources, publishers gain power. Proprietary software may institute features that violate user privacy or block users from doing things with the software that the publishers don't like. Users can only take it or leave it.^[And you may not even have this choice if your work or school requires some proprietary software or textbooks.] By flooding the world with their messages while using copyright law to stop others from building on these now-recognizable cultural references, big publishers control culture, spread their values, and effectively censor ideas they don't like.^[The legal concept of [*Fair Use*](fair-use) is meant to protect such things as parody and satire but generally ignores many other artistic uses including tributes or translations. Fair use is open to interpretation, not respected in all countries, and commonly ignored by powerful interests. On YouTube, for example, unambiguous examples of fair use are commonly subject to burdensome take-down demands.]

    ![](/assets/nina/ME_402_CensorshipVsCopyright1-640x199.png)

    Most physical goods are treated as simple private property. When you own an object, you can legally use it in many ways with few restrictions, including selling it to someone else.^[Legal rights over owned physical goods relates to the *[first sale doctrine](https://en.wikipedia.org/wiki/First-sale_doctrine)*, which was considered a major "safety valve" on copyright in the age of rivalrous media. Of course, regardless of legal issues, much technology these days is designed to be locked-down using everything from non-standard screws to software that uses GPS to make devices stop working if they are moved outside of permitted locations.]

### Funding is great, the other effects are terrible

**Exclusion and imbalance of power hurt the prospects for a free society.** Defenders of copyright and patent laws usually acknowledge this implicitly; their arguments focus primarily on funding.

![](/assets/nina/ME_397_Walls-640x199.png)

Publishers don't say that they want power over readers or want to reduce access as an end in itself or that they want to censor creative derivatives. They claim only that they need restrictions in order to make money and thus to fund authorship. So we need to acknowledge and *address* that one *valid* concern.

## The attention economy

In the internet age, we have many orders of magnitude more works to read, view, or otherwise engage with than hours and energy. So, even as restricted club goods, supply and demand overall pushes the access *price* toward zero.

For artificial scarcity to offset this race-to-the-bottom, it would require massive, wholesale destruction of the public domain and widespread censorship. In other words, sabotage the supply to keep prices up. As long as people continue to publish freely online, artificial scarcity imposed by a portion of publishers will make little difference.

We're already seeing the diminishing of pay-for-access business models. Blocking access leads to obscurity rather than success.

The primary scarce resource remaining is *attention*. So, instead of pay-for-access, **the digital economy now revolves around getting attention from the public and then *selling* that attention to advertisers.**

![](/assets/nina/MimiEunice_59-640x199.png)

But the ad-based online economy today still largely relies on artificial restrictions. After all, when people can share and modify works freely, we happily discard extraneous advertising.^[We suggest [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) as the best all-purpose blocker for advertising and tracking. Furthermore, we suggest also checking the optional "Social" items in the 3rd-party lists settings.] So, copyright laws and DRM technologies that were ostensibly made to create artificial scarcity (i.e. to stop copyright infringement, block sharing, and make each person pay for access) are now used *primarily* to control engagement and push ads more effectively.

So the attention economy remains tied to problematic artificial restrictions. Of course, third-party advertising has many other [awful side-effects](existing-mechanisms#third-party-ads) as well.

## A healthier creative economy

**Snowdrift.coop aims to better fund creative works as true public goods.** We want more accuracy, openness, and honesty in our economic relationships. We want to see the natural free and open flow of ideas, technology, and culture.

### Community-based patronage

Before the era of copyrights and patents, creative people had wealthy patrons who supported their work. We have the same thing today in the form of grants from the foundations of wealthy philanthropists. Of course, relying on wealthy elite funding brings conflicts-of-interest and other problems. So, we need to empower the broader public to become the patrons for creative work. Then, we can direct our resources to the projects that best serve the public interest instead of the interests of wealthy elite or of advertisers.

Exceptionally popular projects, like Wikimedia, already manage well enough with the support of the small percentage of donors who contribute through traditional fund drives or general sustainable donation systems. The Snowdrift.coop [funding mechanism](mechanism) aims to amplify this community-patronage approach to support more projects and convince more publishers to keep their products as true public goods instead of exclusive club goods.

## Further reading

Although a bit dense, the [Wikipedia article on Public Goods](https://en.wikipedia.org/wiki/Public_good) and relevant links there provides additional details on the economic concepts.