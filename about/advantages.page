---
title: Snowdrift.coop Advantages
categories: communications
...

There are many particular benefits to the Snowdrift.coop platform over any alternatives. We have a separate listing of other crowdfunding sites (see [Market Research](/market-research) with comparison and our thoughts. Below are specific items about our system we want to emphasize.

## Matching

Our [mechanism](mechanism) helps network everyone in the community together to reinforce our collective impact. It promises better coordination toward the best projects, and it address the [problems with funding public goods](snowdrift-dilemma).

Users can feel that their pledge made a great difference to the world because it affects how much everyone else contributes.

## Non-profit

The majority of crowdfunding sites are for-profit. They may be more or less ethical, more or less dedicated to the quality of their service, but a profit focus creates potential conflicts-of-interest for services whose primary activity is inserting themselves as middle-men between donors and recipients. Snowdrift.coop's focus is on our non-profit [mission](/mission).

## Cooperative

With most services, you must accept the rules and structure the owners set. Some businesses may be more open to suggestions than others, but it is clear where the power lies. With Snowdrift.coop, everyone can be a voting member electing a board of directors, and any member run for the board or submit petitions for a member vote on any policy. Snowdrift.coop is run by the community. This is the only way to be consistent with the free/libre/open ideals of all the projects we support. Furthermore, our cooperative system will be available for adoption by projects so each project may run cooperatively as well if they choose.

## Strictly FLO

All the ideas, writings, and software behind Snowdrift.coop are themselves free/libre/open, respecting everyone's freedom and making it as easy as possible to contribute creatively and adapt the system as needed.

All the projects at Snowdrift.coop are free/libre/open. Unlike most crowdfunding systems, we do not encourage people to fund projects which are then restricted and disrespect the community's freedoms.

## Ongoing, long-term

Most crowdfunding is done with short-term fund drives. There are a few other ongoing systems out there, but few that emphasize community engagement and accountability the way we do at Snowdrift.coop.

## Accountability

Our holistic system emphasizes accountability by combining on-going funding with honor,  transparency, and more.

## Low cost

As a non-profit, we are interested in minimizing the expenses of the system. Some other services charge significant percentages on top of the cost of transactions. Snowdrift.coop will be funded by member patronage and work to otherwise minimize fees.

## Honor system

Drawn from social science research and the best previous models, our [honor system](honor) gives respect and dignity to everyone while helping maintain everyone's standing and honorable behavior.

## Respectful and effective discussion system

We are using the best ideas out there for encouraging an engaged community while maintaining respectful discourse. This is achieved via a technical system that reinforces our [Community](/community) Code of Conduct.

## Emphasis on discretion and judgment with minimum of hard rules

Zero-tolerance policies and other hard rules encourage a computerized, less human approach to complex situations. At Snowdrift.coop, we favor thoughtful human judgment and provide only guidelines for most potential conflicts. We recognize the value of building a conscientious ethically-minded community. Rules are a means to an end and not an end in themselves.

## Minimal artifice

Snowdrift.coop has no artificial deadlines, no short-term discount sales or other gimmicks, and no unrelated rewards. Our rules are not designed to manipulate people. Our rules serve the purposes of furthering ethical behavior and of simply recognizing the need for everyone to agree together to support worthy projects.

## Open standards and no tracking

Those not wanting to run JavaScript or who have special access needs are still able to use our system. We do not run non-free JavaScript or third-party trackers or anything else questionable. When in doubt, we err on the side of transparency and respect users' privacy and control.

## Respectful, optimized legal policies

Our legal policies are adapted from and/or inspired by the most ethical existing policies from projects such as Wikimedia, Mozilla, and Debian. Every policy has been drafted to be maximally transparent and serve the best interest of the community. We have further worked to make our language accessible and reduce legal jargon as much as possible.

## Integrated ticketing and discussion

On many open-ended forums, discussions may reference bugs and issues that should be tickets. We integrate things so any comment can be marked as a ticket. This keeps things in one place and provides the most flexible and contextual way to handle issues.

## Flexible tagging

We are implementing a tagging system with more powerful flexibility and sort/filter features than are available in most systems.

## The best payment options

*We are dedicated to determining the very best, low-cost, privacy-respecting, ethical payment services (see [Market Research](/market-research)).*

## Technical advantages etc. of Haskell / Yesod

Our system is simple, robust, snappy, secure, and reliable.
