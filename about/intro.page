---
title: 
toc: false
categories: communications
...

*The following is an old intro prior to our current branding. For an updated short intro, see our [60-second intro video](https://archive.org/details/snowdrift-dot-coop-intro). The old intro still provides a good additional perspective with some further details.*

<div style="text-align: center;">

# Intro to Snowdrift.coop

Things like computer programs, art, music, texts, research, data, plans…

These are all *soft* wares…

<div>
<img alt="soft things imagined in a fuzzy cloud" style="padding: 0;" src="/assets/intro/soft.png">
</div>

They take effort to produce but cost little or nothing to copy and share.

![](/assets/intro/effort.png)

<br>

### Today, we have two ways support their development:

<h2 style="margin: 1em 0 0">
Proprietary
</h2>

lock things down,  
charge for access and/or show ads

![](/assets/intro/proprietary.png)

Project teams get paid,
but restrictions block freedoms to access, use, modify, and share!

<h2 style="margin: 1em 0 0">
Free/Libre/Open 
<small>[(FLO)](free-libre-open)</small>
</h2>

voluntary community development

![](/assets/intro/flo.png)

Freedoms for all! Open access! Collaboration!
But limited funding often means slow and fragmented progress.

### In principle, we could get both funding *and* freedom…

The funds that now go toward proprietary access could instead
fund FLO projects…

With livable salaries, core teams could dedicate full time and energy…

<div style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0;">
<img style="padding: 0;" src="/assets/intro/flo-funding.png">
</div>

Volunteers could still contribute creatively, and those who can't afford
time or money would still have access…

## Practical problems

Consider the **[snowdrift dilemma](snowdrift-dilemma)**:

![](/assets/intro/snowdrift.png)

A big snowdrift blocks the road. Who will clear it?  
If we don't trust everyone to do their part,  
then each of us waits to see if *others* will get to work…

### The fundraising dilemma

Simple requests for donations only go so far.

![](/assets/intro/donate.png)

As in clearing the snowdrift, people hesitate to do their part if they don't trust that everyone else will come help.

Changing the economic system requires coordination.

### How about crowdfunding campaigns?

To assure a **critical mass** of supporters, most crowdfunding sites use *all-or-nothing* fund-drives with hard deadlines.

![](/assets/intro/threshold.png)

These one-off campaigns sometimes work but have [several problems](threshold-systems). They are risky and often costly, offer little accountability, and usually rely on special rewards (thus favoring proprietary projects).

## Solving the dilemma

To provide *flexible*, *sustainable*, and *effective* support for FLO projects, a solution will need *both*:

* Ongoing engagement so the community can build trust
* A social contract that provides mutual assurance

## Introducing:

![](/assets/intro/logo.png)

Instead of just donating unilaterally, patrons select projects to support and announce to the world:

<div style="display: inline-block; vertical-align: top;">

<p style="padding: 4px">***Each month,***<br>
![](/assets/intro/month.png)
</p>

</div>
<div style="display: inline-block; vertical-align: top;">

<p style="padding: 4px">***I will donate 1¢***<br>
![](/assets/intro/cent.png)
</p>

</div>
<div style="display: inline-block; vertical-align: top;">

<p style="padding: 4px">***for every 10 others who join me!***<br>
![](/assets/intro/10-patrons.png)
</p>
</div>

We call this fundraising approach *crowdmatching*.


<img style="float: right" src="/assets/intro/wealthy.png">
Of course, patrons have different means, so wealthier patrons can set their match level higher

### Maximum impact, minimum risk

With our system, nobody goes it alone.

Each pledge encourages more patrons to join.

![](/assets/intro/pledge-button-draft.png)

### A network effect

Everyone works together to support the most deserving projects!

<img style="padding: 0;" src="/assets/intro/quadratic.png">
<br>
**more patrons**
&times; **more \$ per patron** = quadratic funding growth

### Budgeting your support

**Each patron controls their own budget.**  
If budget limits are hit, patrons can choose to increase their budget or decide which projects to drop, and they can adjust their pledges at any time.

### Accountability

As long as projects continue to do good work and respond to feedback,  

<div>
they will receive ongoing support,  
![](/assets/intro/ongoing.png)
</div>
<div>
allowing them to prosper and grow.  
![](/assets/intro/grow.png)
</div>

### A democratic platform

We're organizing as a non-profit member-owned cooperative,

<img alt="part of the international co-op movement" src="/assets/external/coop-marque/png/coop_emgr.png" style="height: 6em"> ![](/assets/intro/co-op.png) 

and we will fund ourselves as a project on the site instead of taking a cut from others.

## [Click here to read more about the Snowdrift.coop premise](/about/premise)

### Or jump in and try it…

[Register an account](https://snowdrift.coop/auth/create-account) to pledge. As we develop the system, we'll add more clear charts and stats and more features to help projects succeed and keep patrons engaged.

</div>
