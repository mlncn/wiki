---
title: Limits to Pledge Amounts
categories: communications
...

Our [pledge mechanism](mechanism) imposes no strict boundary on pledge values, but practical limits achieve a natural equilibrium.

## Patrons control their budget

Patrons only donate up to the budget limit they set for the system. If the crowdmatching for the projects a patron supports goes above the budget limit, then their pledges to project(s) that grew past the limit will get suspended. The patron may choose to then increase their budget limit or drop other projects if they want to reinstate the suspended pledges.

## Project growth and stability

**Snowdrift.coop intends to support ongoing development and natural growth towards sustainable project goals.** The particular nature of a project will determine where that growth tapers off.

### Consolidation versus sub-projects

A musician who releases FLO recordings but also earns money through live performances may only need enough Snowdrift.coop patronage to cover composing time and studio costs. On the other hand, a major software project may need dozens of full-time team members.

Smaller projects may find the best fit to the platform through consolidation into a single larger project, whereas huge projects may break into smaller sub-projects. The right moderate size will help maximize total support while staying responsive to the community and engaged on a human scale.

### Sustaining when not growing

If a project reaches a sustainable and adequate level with no further plans for growth, it can simply announce that its needs are fully funded. Patrons can then gravitate towards other projects that have unmet funding needs. Additionally, projects may pass funds on to upstream projects (which often have less ability to get public support directly).

### Projects pausing funding

Projects can place a hold on receiving donations. This status will allow them to engage with users and accept pledges, but not receive any funds. A project might use this option to decline any funding until the total reaches a baseline monthly amount that they need to begin development. In other cases, a project may take a break from development or decide that they have adequate funds for the near future yet may want to continue getting pledges and reopen donations at some later time.

## What if the pledge value gets too high? 

**We will have succeeded already if the problems we face involve high pledge values**, but we still care about keeping the cost low enough to allow maximum participation.

First, because we calculate pledge value by the number of patrons, we can only see high values with a large number of patrons who can all afford it. As a pledge only counts if it fits in the patron's budget, the dropping of over-budget pledges automatically counteracts the growth of overall value.

We want patrons to consider increasing their budget as the values grow (assuming the project's output improves correspondingly), but patrons do have some budget limit to what they can possibly afford. So, we could offer ways to support continued participation at lower levels. Although we don't plan to implement features for this right away, one possibility could be a reduced-frequency pledge option set for bimonthly, quarterly, semiannual, or annual donations. As stated above, **splitting into smaller sub-projects is another way to allow continued participation at lower donation levels**.

Once a project is truly successful, we could even begin allowing donations that are not connected to the crowdmatching. After all, the entire purpose of crowdmatching is to coordinate people to reach the levels projects really need to reach their potential. We can consider alternative options if we succeed at that initial goal.

## What if pledge value stays too low because of too few patrons?

Our system works best with relatively large pools of patrons. Specialty, niche projects with a smaller audience do not fit as easily with the default pledge. One simple feature we could support: projects could set their basic crowdmatching pledge at a higher base rate. Or, a corollary to keeping pledges affordable by reducing payout frequency: we could *increase* frequency to semi-monthly or weekly as another mechanism for *increasing* funding for smaller niche projects. We will implement such options if necessary.

## Room to adjust as we go

Overall, we will discover the best options and pledge details through the natural bottom-up system as it operates over time. Patrons will come to understand that some projects naturally reach a wider audience than others, and a balance will be found for how to best support different types of projects.

Projects that struggle to achieve necessary support in our system are likely projects that do not have the necessary foundations to succeed. Overall, we wish for the limited funding and energy of the whole community to be put toward the most deserving and effective projects. While we want as much success as possible, we'd prefer a smaller number of truly successful projects over a large number of fragmented struggling ones.

As a cooperative, the community can decide democratically about any adjustments needed to fulfill our mission.
