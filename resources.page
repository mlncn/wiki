---
title: Team Resources
categories: project-management, help
...

This page lists the main resources the Snowdrift.coop team uses to organize our work.
For a high-level overview of our upcoming goals, see the [roadmap](/planning#roadmap) instead.

## Project Management, Development, & Design

Places to check as we track our work:

- [Gitlab](https://gitlab.com/snowdrift) — our primary issue tracker, with several repositories for different areas of work
- [forum](https://community.snowdrift.coop)
    - individuals can [bookmark topics](https://community.snowdrift.coop/my/activity/bookmarks) for reference
    - the restricted [drafts category](https://community.snowdrift.coop/c/restricted/drafts/) holds drafts of posts we plan to finish and publish
    - topics can be [assigned to you](https://community.snowdrift.coop/latest?assigned=me) to address
- [blog drafts](https://blog.snowdrift.coop/ghost/) (unpublished work in progress at the [blog](https://blog.snowdrift.coop/))
- carried-over notes from meetings (see below)

## Meetings

* [General meeting info](/resources/meetings)
* [General Circle meeting notes](/resources/meetings/2019/)
* [Website Circle meeting notes](/resources/website-meetings/)
* [Outreach Circle meeting notes](/resources/outreach-meetings/)
* [Board meeting notes](/resources/board-meetings/)

### Wiki documents

* [Communications](communications) — communication strategy, presentations,
interviews, and other publications
* [Community](/community) — community partners and volunteering information
* [Market Research](/market-research) — research other funding platforms, FLO
  tools, repositories, and more
* [Legal](/legal) — bylaws, incorporation, board and other legal policies
  (moving soon to the [Legal repo](https://gitlab.com/snowdrift/legal) on GitLab)

---

## Misc Tools

- Pastebin: [lpaste](http://lpaste.net/)
- Imagebin: [42.meup.org](http://42.meup.org/) | [Framapic](https://framapic.org/)
- Etherpad: [etherpad.net](https://etherpad.net/) | [Riseup Pad](https://pad.riseup.net/) | [snowdrift.sylphs.net](https://snowdrift.sylphs.net/pad/) (snowdrift internal use)

### Team availability tools

We use WhenIsGood.net currently^[We've even paid their stupid ransom to access the extra features. *Please* someone create a FLO version of this! We spent a *stupid* amount of time fussing with ways to coordinate people around the globe sticking to only FLO tools.]

[Framadate](https://framadate.org/) is a FLO tool for this, but it has the wrong UI and does not serve our needs like WhenIsGood.

The forum has a calendar plugin we haven't explored and a time-and-date function we can use for some things.

### Adding a resource file

1. [Upload](https://wiki.snowdrift.coop/_upload) the file to `/resources` by giving the path `/resources/filename.ext` for the name (replacing "filename.ext" with the desired filename and extension).

2. Add the link to the list above.
