---
title: Project Requirements
categories: governance, project-liaison
...

*Note: these requirements are being updated and adapted in a file in our [legal repo](https://gitlab.com/snowdrift/legal/blob/master/project-reqs.md)*

## Required licenses and FLO definitions

**Free/Libre/Open (FLO) means unrestricted freedoms to use, modify, and share**. A few precise FLO definitions compete for recognition as the standard in different fields.^[The different definitions are generally between "freedom"-focus versus "open"-focus, which involves the same terminology debates surrounding the term Free/Libre/Open.] Thankfully, the most accepted definitions are practically equivalent. We do not want to create yet another competing standard, so we defer to the options listed below.

*Note:* we have a separate page in the [About](/about) section, titled "Why Only Free/Libre/Open Projects?" discussing *why we limit the site to FLO projects*.

### Software

Software projects must meet either of these definitions:

<div class="row text-center">
<div class="col-sm-6">
[Free Software Definition <img alt="GNU logo" src="/assets/external/gnu.png" style="height:6em;">](http://www.gnu.org/philosophy/free-sw.html)
</div>
<div class="col-sm-6">
[Open Source Definition <img alt="OSI logo" src="/assets/external/osi.png" style="height:6em">](http://opensource.org/osd-annotated)
</div>
</div>

### Educational, research, and cultural works

Projects in the fields of education, research, journalism, art, video, and music must meet either of these definitions:

<div class="row text-center">
<div class="col-sm-6">
[Definition of Free Cultural Works <img alt="FCF logo" src="/assets/external/fcf.png" style="height:6em;">](http://freedomdefined.org/Definition)
</div>
<div class="col-sm-6">
[Open Definition <img alt="OKF logo" src="/assets/external/okf.png" style="height:6em">](http://opendefinition.org/od)
</div>
</div>

### Online services

Online services must meet the [Open Software Service Definition ![open service logo](/assets/external/open-service.png)](http://opendefinition.org/software-service)
and should otherwise abide by the [Franklin Street Statement on Freedom and Network Services](https://wiki.p2pfoundation.net/Franklin_Street_Statement_on_Freedom_and_Network_Services).

Overall, online services should relate in a fundamental way to networking and communication and not merely substitute for products that would be equally effective when run locally and offline.^[The GNU.org article [*Who does that server really serve?*](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html) is useful for further consideration. That article describes the problems with Service as a Software Substitute (SaaSS).]

### Hardware plans

Snowdrift.coop focuses on funding the production of non-rivalrous goods. Projects that produce hardware are still welcome as long as funding from Snowdrift.coop is aimed toward serving the non-rivalrous elements of the project.

Projects working on plans, software, designs, and related resources for hardware must meet either:

<div class="row text-center">
<div class="col-sm-6">
[Respects-Your-Freedom hardware criteria <img alt="RYF logo" src="/assets/external/RYF.png" style="height:6em;">](https://www.fsf.org/resources/hw/endorsement/criteria)
</div>
<div class="col-sm-6">
[Open Source Hardware Definition <img alt="OSHW logo" src="/assets/external/oshw.png" style="height:6em">](http://www.oshwa.org/definition/)
</div>
</div>

### License options

Each definition above includes connected lists of approved licenses. See our separate discussion of [FLO license options](licenses).

<img alt="Copyfree" src="/assets/external/copyfree.png" style="float: left; height:5em; padding-right: 8px;">
The list of [Copyfree licenses](http://copyfree.org/licenses/) from the Copyfree Initiative includes several legitimate FLO licenses that have not been reviewed by the organizations above, so we will accept any of those licenses as well — as long as the definitions above are still otherwise met.

## Patent non-aggression

All projects which involve any [patentable subject matter](https://en.wikipedia.org/wiki/Patentable_subject_matter) must accept a pledge that the developers themselves will not use patent claims to restrict the freedoms otherwise included in the FLO definitions above.[^patents]

Although not required, we also *suggest* that such projects use a license that includes explicit patent grant such as:

* Version 3 (or later) of the [GNU](https://www.gnu.org/licenses/) GPL, AGPL, or LGPL
* The [Copyfree Open Innovation License (COIL)](http://coil.apotheon.org/)
* The [Apache License v2](https://www.apache.org/licenses/LICENSE-2.0.html)
* The [Mozilla Public License](https://www.mozilla.org/MPL/2.0)

## Product release requirements

* Products must entirely meet the definitions above
    * We do not accept projects which release only limited versions of their work under the FLO definitions while producing a separate proprietary version.
        * Of course, permissively-licensed products are susceptible to derivatives being made proprietary. We do not exclude projects merely because someone else has made a proprietary derivative. However, the project team itself must not actively promote the use of proprietary derivatives.
    * Although we strongly discourage it, project teams may make fully-distinct, *independent* products that do not meet our requirements, but such products shall not be promoted, funded, or otherwise supported through Snowdrift.coop.
    * A project itself can meet these requirements even if typically used in a proprietary context (such as an otherwise FLO software program that runs on a proprietary operating system).
* Release your final results in [standard non-proprietary formats](formats-repositories#file-formats).
* Provide unobstructed public access to anything fully published.
    * Do not force users to do anything extra (such as registering with your site) in order to download the product.
    * For high-bandwidth items, limits on direct access are fine *if* alternative access is provided via BitTorrent or other unrestricted mirrors.
* Documentation and support materials must themselves meet the project requirements.

## Transparency

* Meet our [transparency requirements](/project-reqs/transparency) including reporting status on the items from the [Project Honor Code](/project-reqs/honor-projects).

##  Use of funds raised through Snowdrift.coop

Development should result in concrete end-products and/or improvements in existing products. **Use funds from Snowdrift.coop for ongoing progress rather than for profits or compensation for past work.**

Projects should maximize efficient use of funds. Hire as many quality team members as can be put to good use and as funds will permit while aiming to provide a [living wage](https://en.wikipedia.org/wiki/Living_wage).[^compensation]

Appropriate uses of funds:

* Payments to team for ongoing work
* Covering necessary expenses, including incidental bandwidth/hosting costs ^[Costs considered *incidental* are those that are only a small fraction of overall development expenses. At this time, we do not support the use of Snowdrift.coop funds to pay the expensive hosting costs for especially high-bandwidth items (such as large quantities of streaming HD video). We encourage distributed services like BitTorrent as much as possible. Otherwise, we encourage per-use charges for rivalrous goods like server bandwidth or physically-shipped media. Keeping these costs direct encourages a more honest economic market, better transparency, and conservative use of limited resources.]
* Saving funds in accounts budgeted for future costs
* Donations to upstream FLO projects [^upstream]

[^patents]: We are considering requiring the [Defensive Patent License](http://defensivepatentlicense.org). The initial version had a fatal flaw excluding "clone products" from the licensing guarantee. The newer v. 1.1 seems better but has some burdensome acceptance procedures which sound like there must be a process for each acceptance of a license from an entity. So it does not appear to serve the generalized need we have here. Perhaps the [Open Invention Network](https://www.openinventionnetwork.com/) is better and good enough to require for all patentable-subject-matter projects. The OIN focuses on the Linux community but is for anyone and is very easy to join.

[^upstream]: Any project that is directly related or has significantly contributed to your development can be considered 'upstream'. For example, Snowdrift.coop is an upstream project for others on the site because we provide a supporting service.

[^compensation]: Compensation rates for team members can be controversial and complex. Our ideal is a decent living for all.

    Of course, a project with limited funding must decide whether to support a larger team only part-time (thus requiring them to find additional outside income, either through unrelated jobs or from other FLO projects) or to provide a higher level of support to a smaller team. While we encourage paying a living wage, a projects with limited income must make tough choices.

    On the other side, projects may choose to pay higher competitive market rates in order to recruit and retain the best teams who might otherwise work on proprietary projects. We encourage projects to find teams with lower cost of living in order to make the most of limited resources, but each team may handle these decisions in their own way, subject only to transparency requirements and continued support from the patron community. As a general guideline, projects should recruit the best teams by emphasizing a decent living, autonomy, challenge, and social purpose instead of focusing on the highest pay or other luxuries.
