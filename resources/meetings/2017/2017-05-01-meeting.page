# Meeting — May 1, 2017

Attendees: Aaron, Iko, Michael, Salt, Robert

---

## Agenda

### LFNW

- Salt: I knocked us down to 1 room, OSI is being sent and should be at the hotel when I get there
- I'll be there Wed, at the hotel Thu to Sat
- I'm still working on my talks
- wolftune: I'm planning to bring snowdrift booth-related stuff. I'm going to
  be coming up with Athan and a new programming student, Hayden
- chreekat will be coming up independently because he wants to attend Indie Web
  Camp
- OSI is going to send us $400 to cover expenses, we'll let people decide on
  things the funds will be used
- I'd like to spend some time to figure out how to explain to people our
  current status, challenges, next steps, etc.
- mray: I don't have any insight on what's going on there, so maybe you can
  decide
- wolftune: it's a Linux-centric conference with programming-proficient people
- it'll be ideal to tell people the steps to take, e.g. sign up on
  Discourse/ML, ways they can help
- imagine we meet someone with some Haskell experience, a Linux user and
  supports what we're doing
- what would you say?
- mray: I think this relates to how we direct people online to the right tasks,
  how to be constructive project members
- our internal tasks and tools aren't settled completely
- it may be better to focus on interaction, to invite people to get in touch
  with specific people (directly to a person), instead of a document, until we
  have the structure to redirect people
- I would be willing to connect with people in this capacity, I don't know
  about others
- wolftune: it's like what I've been saying about pair programming,
- some of this is about chreekat getting clear about technical tasks
- also signing people up and getting them onboard. I wonder if we should do
  intake sessions, a live chat?
- we can schedule it a week ahead, with chreekat present
- Salt: I'm not sure about it being developer-specific, but a real-time
  conversation (1-2 h) would be really valuable
- wolftune: a webinar, is that what people call it? we can tell people at LFNW
  to sign up for webinar to happen later
- Salt: do we want to schedule it now, or make it a task to schedule it?
- wolftune: we want to make sure people know what they're signing up for
- Salt: we can work on the pitch while we're at the festival
- **Next step: wolftune sends out email to ML to schedule webinar**

### SSO status?

- Salt: does anyone know the current status?
- **Next step: Salt asks MitchellSalad about status**

### Civi etc.

- wolftune: what's the status of CiviCRM?
- Salt: I've not had time to work on it, but I can walk you through it during
  LFNW. everyone has accounts
- wolftune: has anyone else used CiviCRM besides Salt and myself?
- mray: I have, while helping to merge contacts in the databases
- wolftune: everyone is welcome to be proactive and add contact people to
  CiviCRM
- iko: any criteria for adding contacts?
- Salt: future interaction, e.g. wants to help but will be busy for 2 weeks,
  you can add a reminder to get in touch with them again in 2 weeks
- wolftune: as another example, alyssa is the new head/org lead for Libreboot
  who's interested in the project and gave some feedback

### Getting tasks/issues from chreekat and GitLab Issues

- wolftune: this is about getting chreekat to enumerate dev tasks that people
  can help with
- he has a list of notes to himself of things to do that we haven't gotten into
  Taiga or posted somewhere yet
- mray: we've been talking about possibly moving to GitLab Issues. maybe we can
  introduce chreekat's list there?
- Salt: it is very difficult to track things in two/multiple places
- wolftune: yeah, I agree. I have a feeling chreekat would like to go to GitLab
  Issues
- if that goes well, then I don't mind consolidating there
- mray: I think I'm pro-GitLab even though I haven't really tried it yet
- wolftune: are there people who have been able to use Taiga for tasks or are
  we all looking at it in frustration?
- Salt: I'd be in the latter group
- mray: I didn't have a really good experience with Taiga. it's not so
  intuitive, tasks have to be set up/added a certain way
- wolftune: I feel the same way, burden of the JS stuff and clunky UI, no
  threading and clear replies
- iko: no major problem with Taiga (can sympathise with the concerns though),
  no problem with switching to GitLab Issues either
- however, to second what smichel17 said before in irc, it takes practice to
  get used to the tools. if part of the issue is a human element, where e.g.
  people simply haven't had the time/opportunity to practise using the tools,
  then switching tools isn't going to resolve it
- Salt: I agree, though I also see value in keeping Taiga around and using
  GitLab Issues for new tasks (considering in addition the amount of work
  setting up and importing data)
- **Next step: get coding stuff in a place where people can actually use it**

### Video script meeting?

- wolftune: I got some feedback from alyssa that I'd like to discuss
- mray: do you have a date in mind?
- wolftune: today is open, tomorrow I'm open before 5:30 pm PST
- **Next step: mray and wolftune get in touch after the meeting**

### Carried-over next steps

- Target audience and personas
    - wolftune: that's still something I'd like to do
- Blog
    - iko asks smichel17 to help add tasks in Taiga for getting blog in
      production?
- Discourse
    - Salt follows up again with MitchellSalad
- PRs
    - Salt speaks to chreekat in person
