# Meeting — February 20, 2018

Attendees: mitchellsalad, mray, MSiep, Salt, smichel17, wolftune, iko

# (PSA) New mailing list landing page
- iko: new landing page is ready, if there are no other changes
- salt: let's not make the change until we're ready (to launch Discourse), but otherwise looks great, thanks
- wolftune: for reference, this will replace the landing page at lists.snowdrift.coop as part of migrating from ML to Discourse
- NEXT STEPS: (pending plans from outreach circle meeting Feb 23) when ready, open MR at <https://git.snowdrift.coop/sd/lists.snowdrift.coop>

# smichel17 — "let me know where I can be useful"
- smichel17: I have no real big projects/priorities for snowdrift
- I'm a generalist and have the background to pick up the skills to work on pretty much any aspect of the project
- So, if you feel like you have a lot on your plate and want someone to help you out, reach out to me
- wolftune: I have stuff!
- NEXT STEPS: wolftune delegates stuff to smichel17

# Conferences coming up
- wolftune: we want to keep track of people who will be at different conferences, and connect with people interested in representing Snowdrift.coop at conferences
- salt: I'll also be at LibrePlanet
- mitchellsalad: I'm going
- wolftune: what are our next steps to be conference-ready?
- salt: we can arrange an outreach circle meeting
- NEXT STEPS: get civicrm up before conference season
- NEXT STEPS: arrange an outreach circle meeting between wolftune and Salt, other outreach circle members welcome to attend
- **Outreach circle meeting: Friday, February 23 at 12:30 PM PST (3:30 PM EST)**

# Blogging
- wolftune: feedback on how much to prioritise blog topics, what kind of content should go out sooner?
- salt: do you have a concrete list?
- wolftune: not really; it's spread across a bunch of places
- salt: best place to put such a list might be in the forum blog category, as a draft
- NEXT STEPS: start a list (on a wiki page or discourse topic) with a list of potential blog topics

# Board recruiting
- wolftune: I'm starting to put together lists of people who might be interested for the board, along with challenges involved
- wolftune: just wanted to open up the floor to questions if anybody wanted to ask about it
- NEXT STEPS: n/a

# Updated (and updating) governance
- wolftune: I've been doing an overhaul of the governance docs
- chreekat had questions about structuring by discipline, e.g. engineering and design team, vs. the current way by product
- if we make a significant change, it might be in that direction
- smichel17: I would prefer to have that discussion when chreekat is around
- I think what we currently have is working for the website circle, so not inclined to touch it, only in places where things aren't working
- NEXT STEPS: n/a

# Site engineering
- *(mitchellsalad wants to know what to do)*
- salt: sounds like something we discussed with chreekat last time, how can volunteers know what devs can be worked on and where
- wolftune: so far chreekat has marked some tasks in gitlab, e.g. projects page as "MR welcome"
- "MR welcome": <https://git.snowdrift.coop/sd/snowdrift/issues?label_name%5B%5D=merge+request+welcome>
- smichel17: things are still being loaded into the pipeline, most are still at the US/design stage
- wolftune: chreekat also has a list of ops tasks, if you can help with them <https://community.snowdrift.coop/t/website-development-roadmap/471>
- NEXT STEPS: keep an eye on the "MR welcome" tag

# Discourse categories
- salt: this will be partly based on the meeting with the outreach circle, but do people have any feedback?
- wolftune: there's been discussion around using tags vs. categories
- smichel17: there are some decisions that are waiting for you when you have time, or you can delegate to someone else
- NEXT STEPS: Salt to review and respond to Discourse topics on these things


# Carry-overs

```
## Describe a process for how we’ll use CiviCRM to enter and tag prospective first projects
Assigned: Salt

## Recruit projects
- NEXT STEPS: consolidate our list of potential partner projects
- NEXT STEPS: GHC as incubator project? -> add idea to civcrm or wiki?
Assigned: wolftune and/or Salt
```