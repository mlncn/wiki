## Meeting 3

**Saturday, April 6, 2013. 12PM Eastern**  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and etherpad: http://etherpad.wmflabs.org/pad/p/Snowdrift20130309*

### Agenda

* review of minutes from [meeting2](meeting2)
* welcome and introduction: James Sheldon as new committee member
    * input and work on organizational details, committee process, etc.
* report from Aaron about the status of things and progress over the month
    * work on presentation materials
    * legal filing
        * non-profit multi-stakeholder cooperative
    * HTTPS implementation
    * more ideals: ripple
* report from David about technical status
    * recruiting status / thoughts re: other developers
    * status of project-in-project process, built-in login, plans for mailing list
* Discussion of priorities, planning next steps
    * bylaws and cooperative structure
        * who are stakeholders, other issues, process for development
    * payment-processors
    * how to prioritize and focus on concrete steps and deadlines
    * getting the committee to check in with and hold accountable those active working to accomplish certain goals
    * considering pursuing grants or funding options for our medium-term development to get to live functioning stage
* any other business / discussion
* adjournment

---

#### Minutes:

* Aaron: overview, minutes, notes since last time  
    * technical: https, project-in-project  
    * role of committee: advisory, but please hold us accountable  
    * visionary/realist/critic framework:  
        * feedback to visions should address visions, not practicality  
        * feedback to plans should address practicality
    * Code for America mentioned in previous minutes (no progress)
    * payment discussion
    
* James: basic introduction, excited to join
* Aaron: James seems to have particular interest in organizational form and structure
* James: mostly observing now, thoughts about things interact, learning about coop legalities
    * background
        * worked with many groups
            * student activist groups
            * religious groups
        * looking at open source world
            * disconnect with groups that should be sympathetic
            * accessibility
        * work in education
            * access to diversity of materials restricted by copyright
* Aaron: status update
    * working on presentation
        * refining since LUG talk
        * wants to put together slideshow and/or video
            * some progress
            * step toward being more public, lower barriers, get people understanding idea fast
* Greg (typed on etherpad): have you looked at opendesignengine.net?  It has some similar components to what you want to visualize on a per-project basis
    * also we recently made the front page for our project here - http://photosynq.org/ .  It's not professional, but it may be decent to look at to start
* General talk about current status and next steps, Nina and Greg emphasize dropping the hesitation toward being fully public, need an accessible homepage that means something;
* Nina going to Libre Graphics conference in Barcelona, will be able to mention things there, but recruiting is easier with more accessible, simpler landing page to explain the project.
* Nina: not sure how to contribute at this point, expertise is not in the backend or organizational/legal stuff, wants to see the point where a test site is functional and then can give feedback on experience and structure from the project perspective
* David (typed on etherpad): Some up-front donations *would* help free up developer time, for people who need to pay bills.
    * Aaron ads: this is all of us really, but not sure how to go about it except that better landing page and presentation and accessible site will help interest supporters
* Aaron: Concerns about early openness
    * Rest: Encouragement of early openness
* Aaron: mention of ripple
* Aaron: Need to study funding options
* Discussion of virtual site - let's get things running with fake money
    * We should accelerate the payout process, so run daily instead of monthly maybe
    * This way we can see how things work and figure out details of the system aside from real money
    * Connected to making it easier to register accounts, as we need more people pledging to have the testing work
* Aaron: we've got our Articles and docs sent to the State of Michigan, this very morning
    * next: bylaws, rules, ToS, &c
    * have some drafts from edits of existing bylaws
* James: need to be looking at other coops in particular, not just other nonprofits
* Greg (on etherpad) --> here's a multi-stakeholder non-profit (not sure if it's a coop or not) but it does have the articles of inc. there and I have a good friend whose been involved in it for quite a while and can answer more questions if you have them [http://rsb.org/about/organization/](http://rsb.org/about/organization/)
* Landing page - **Aaron will have something one week from today.**
