# Meeting — August 10, 2016

Attendees: salt, iko, smichel, chreekat

---

## Pre-meeting discussion

- there was an issues triaging session prior to the meeting where 
chreekat sorted through the Issues section and prioritised items with new 
priority categories smichel set up.

- a question was raised about the scope of sprints and what it means for teams 
like devops that have ongoing tasks (e.g. setting up Discourse) that aren't 
related to the current milestone (master-to-production). this led to a 
clarification on sprints: a sprint lets teams share what they are currently 
working on. it's unrelated to mvp or milestones.

Process to add team items to sprint board:  

- team has a list of their own priorities, things they want to do
- at the sprint planning meeting, team brings list to meeting along with 
dependencies/requests for other teams
- as long as there isn't conflict with other teams, then items will be added to 
the sprint board

---

## Checkin

* Salt: obtained wildcard cert, community.snowdrift.coop subdomain is set up
* iko: waiting for sprint board to be populated/ready
* smichel: 
    - excited about progress with governance doc changes
    - on vacation next week, wrapping things up before going away
* chreekat: need more concrete understanding of timeline/breakdown


## Agenda

### Staying focused (chreekat)

Identified problems (Salt):  

1. Overall nature of setup - a lot of unimportant issues in the backlog
2. Sprint focus - things that need to be done by a certain date to keep us on 
track

- chreekat: scrum exists to provide focus so people know what they should be 
working on
- smichel17: the current work Salt and I are doing should help
- Salt: we're going through governance docs, removing accountabilities and 
moving to shorter, purpose-driven roles


### Taiga Issues 

Revised status categories:  

- *New* - new IS reported
- *Ready* - a Scrum Master has triaged it issue is ready to be worked on. 
triaging basically means it has been reviewed and prioritised. note that it is 
different from grooming, which is working with team involved to clarify 
questions, implementation, etc. and applies to US, not IS.
- *In progress* - a volunteer is working on it. it doesn't have to be promoted 
to a US. If it's not promoted, it's currently not on the core team's roadmap
- *Closed* - IS is done
- *Rejected* - wontfix or IS will not be done for other reasons

Discussion  

- iko: what is the difference for a core team member working on an Issue and an 
issue that has been promoted to a User Story?
- chreekat: one is a US, a mini-project with sub-tasks. It usually has personas 
with full goal and purpose that can be accomplished, whereas an issue can be 
different things.
- smichel: an IS that is promoted is on the core team roadmap?
- chreekat: I wouldn't turn an IS into a US just to put it on the roadmap
- smichel: if you want to work on an issue but you don't want to turn it to an 
US, then you would make it a task on a sprint?
- chreekat: yes

- iko: with the references to a roadmap, where can we find it?
- chreekat: we don't have one, it's the combination of having milestones and 
sprints
- chreekat: the closest thing right now is wiki.snowdrift.coop/next and MVP
