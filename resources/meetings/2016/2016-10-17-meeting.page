
# Meeting — October 17, 2016

Attendees: chreekat, mray, MSiep, iko, Salt, wolftune

---

## Checkin
* chreekat: hard at work on mechanism, finished prototype last Tuesday, still working on "the real deal"
some architectural changes but it looks like there is a clear way forward
* mray: finished work on html/css so far, starting to think about making the intro animation
* MSiep: busy and just got caught up with ML messages
* iko: nothing to report this week
* Salt: finished setting up and upgrading CiviCRM, working on Discourse, running out of time (leaving Wed/Thu)
* wolftune: sorry I'm late, the timing just didn't work out

## Agenda
- Intro video script (MSiep)
- Moving Nov 1 date (Salt)
- Elections (Salt)

## Intro video script
- MSiep: I feel like it could be more story-based, less academic. I started
  drafting something this morning but not finished yet
- with illustrations it's an opportunity to tell a story, I will share an
  example soon
- Salt: mray, how much work will it be for the longer 1-min script? Too much
  work?
- mray: too much, I wouldn't like to proceed without being sure we have to go
  that route. it's not really feasible
- MSiep: what sort of animation do you have in mind? like a character walking
  about … ?
- mray: I'm thinking about how the parts fit together, in the form of M&E
  reacting to what he is talking about
- I'll proceed after there's a clear goal (to the video), which is lacking at
  the moment
- MSiep: agreed, too much work invested -> more resistance in changing it
- Salt: agreed. however, without Aaron here in the meeting, it's hard to
  discuss the fine details
- mray: I had a long and constructive video chat with him
- chreekat: I agree people should use the right tool for the task, although
  usually I'd encourage people to use email more
- a comment on-topic, I feel like we're talking about 2 different sites. with
  the snowdrift dilemma/metaphor. we should probably be called crowdmatch.coop
- we don't assume people understand the problem right away, but to be able to
  understand the problem based on the solution being explained
- it's good to think of the problem separate from the solution
- I'm not saying we change the name
- MSiep: if it's not that much, we could think about getting the
  crowdmatch.coop domain as well
- Salt: not a bad idea
- chreekat: crowdmatch.com and crowdmatch.org are taken, probably a squatter

(wolftune arrives and Salt summarises the discussion so far, including MSiep's
idea)

- wolftune: a story is the way to go in a sense, as long as it doesn't leave
  people in the wrong direction
- I made a note to myself to draft a communications policy
- summary: the snowdrift dilemma of how we clear the road, isn't even a
  metaphor in the sense that's just an example
- we're talking about a class of economic things where most are not concrete
  enough to talk about, so the example is a way to make it more concrete
- the example is "here's how we deal with it" and that same solution works for
  all classes of things that have the same parameters, namely public goods
- so the policy will emphasise not overstretching the metaphor, define some
  consistent vocabulary that we use
- if people have concerns or feedback on how to do this, I'd be happy to hear
  them
- mray: my concern is snowdrift dilemma being part of this approach. on one
  hand, it lends itself to discussion on a train of thought but ties together
  things that don't mix
- I feel there needs to be some untangling and clarification, esp. the
  snowdrift dilemma as a thought experiment and the various outcomes
- wolftune: it's an example of the problem of public goods, it's made simple to
  but it's part of a greater problem. the question is whether we focus on one
  particular problem, or is snowdrift dilemma a greater
- the direction I'm looking to take it: it's inspired by game theory, but we're
  focusing on the public goods problem, how to clear the road
- I don't want to introduce to people the game theory context (yes, people are
  free to discuss it) or some snowdrift dilemma game
- mray: I feel bad about coming for the dilemma and staying for the metaphor
- wolftune: it's not a metaphor
- mray: but that's kind of dishonest, if someone asked the origins, you'd have
  to tell them about the story?
- wolftune: there are some comp sci types that are interested in the
  game-theory side of the topic, but most people aren't interested
- the website isn't about the game, it's about funding real-world issues, and
  the game is one way to think about it
- some people accept the game rules, some people question and don't accept. I'm
  not saying people must accept the rules
- mray: by referencing the game, we have an obligation to stick with it and not
- wolftune: example, adding taxes, if out of these 3 outcomes, only one that's
  best for everyone is the only one that's legal
- mray: in my eyes it doesn't work any longer. why don't we add other things?
  [...]
- Salt: I think it's getting in a bit of a circular discussion, let's continue
  this discussion after the meeting

## Moving Nov 1 date
- Salt: I'm returning on the 7th, SeaGL is on the 12th and we could do an
  announcement there
- wolftune: I like the idea of launching at SeaGL
- Salt: that way people's eyes are on the SeaGL hashtags
- I'm suggesting the 12th, Saturday, I'm giving a presentation at SeaGL
- I'll send out a message on the ML with the suggestion

## Elections
- Salt: I'll be out of town for 3 weeks, and would obviously prefer to not have
  elections during that time
- I'd like to get the Holacracy meetings going again, even if it's just once a
  month, for housekeeping
- do we post to ML or can we tentatively say something here?
- I'm proposing we have elections on Nov 14, at a time when people can make it?
- Can someone (who is not me) organise this?
- Next steps: check with smichel17 or other members?
