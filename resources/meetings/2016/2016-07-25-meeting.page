# Meeting — July 25, 2016

Attendees: chreekat, mray, MSiep, wolftune, iko

---

## Agenda

- Making a sprint (chreekat)
- prototypes (mray)
- mockup email (see "[Snowdrift-design] Visual dashboard idea mockups" sent to ML on July 25) (MSiep)
- "news" and discussion about going forward (wolftune)

---

## Sprint

- Problem: newcomers to the codebase only get the dev.snowdrift.coop code, not the production site
- Some things already moved over, e.g. wiki
- Resources and efforts would need to be focused on getting things to production
- Things to be done:
    - blog
    - Discourse?
    - how-it-works - design and communications team
- Benefits: 
    - visibility (changes to the website
    - 3rd parties can see something is happening)
    - consolidation of code for easier bugfixing (not fixing in two places)
    - collaborating to work more efficiently

## Prototype

- mray: latest prototype at https://snowdrift.sylphs.net/prototypes/mray/#home_page - is it complete?
- wolftune: only thing missing from mvp that should be available on the dashboard is information about history of actual expenses
- mray: new tab "transactions"?
- wolftune: yeah, either "transaction" or "history", you've been a member this long, you've contributed this total
- mray: was there a reason this was not initially included?
- wolftune: we didn't explicitly decided to exclude it, but we didn't discuss it in detail
- mray: I thought we were being strict about paring down to only the necessary for mvp … anything else?
- wolftune: maybe some places where we could add links to the wiki, but that's it
- MSiep: what about a place to put in financial information? credit cards, etc.?
- mray: good question. I have no idea what happens after reaching the limit and user opts to increase limit
- chreekat: I'm fairly sure users don't have to be asked to create Stripe accounts then come back and input the info here.   
instead, we do managed accounts, we will pass some token onto stripe for x person, Stripe will return output that it's x person with x info
- wolftune: we won't be keeping cc info, Stripe keeps the info and we get an agreement saying it's okay to charge x amount
- chreekat: Stripe will have its own widget
- MSiep: UI to change cc is separate from changing limit
- chreekat: I think mray is saying we have financial info from Stripe, but not the limits info
- wolftune: we can do pre-authorisation, it's how Kickstarter works. if someone wants to set amount  
we'll be able to have checks whether that is possible financially
- mray: to clarify my understanding about setting up an account. I need to do 2 things: 
    1. limits to spend each month at snowdrift.coop
    2. set up my bank account at stripe for snowdrift.coop so it can get the money
- mray: there's the problem, if I set limit at $10 but didn't set up bank account
- chreekat: it's up to you how that displays. but we're starting with a new user not matching anyone yet
- MSiep: but if $0 all pledges will be autosuspended
- wolftune: in order for pledge to be valid, we need two things: budget set and valid info
- mray: what happens if both not provided?
- wolftune: pledge can't go forward
- chreekat: let's move on for now and discuss this further later

## News

- wolftune: our co-founder, David, is funding a lead developer for the next 2 months. after that, he would like to fund via a working pledge system  
it's a challenge to get everything launched in 2 months. we may need to look into grants, etc.
- MSiep: we don't want the project to run out of money when there's people out there willing to contribute but we have no way to accepting contributions
- mray: simplest way to do a bypass way to contribute is to have a Paypal link on the project page
- wolftune: sure. another question for later is, we could be launching, but do we want this extra part to be there?

## Pledging/unpledging mockup

- MSiep: this is based on mray's mockups last week with a more linear depiction
- I thought it could be more intuitive, like either the projects fit in a container [of pledge amount] or it can't
- if a project gets suspended, it gets bumped out of the container
- it's more visual compared to a table
- it'll obviously be a bit more complex to code the UI and properly size the elements
- the overall scale will be optimised each display [responsive], inside the projects are sized depending on proportion of pledge money
- mray: I like the idea and started exploring it a little, but for mvp, a simple bar will do
- MSiep: I thought about a way to unpledge, but there's a good argument for having unpledge only happen on the project page
as a relationship between project/user, to give the project a chance to connect/remind the user
it's more convenient to unpledge multiple projects from the dashboard
mray: while there are benefits to slowing down a destructive action, there's a point to giving people a certain amount of control from the dashboard.  
this may feel too much like making people jump through hoops
- MSiep: it reminds me of breaking up romantic relationships via texting, I think it may encourage a healthier interaction with projects
- wolftune: I had a post-mvp idea to display a comment box for people to optionally and anonymously explain why they're unpledging
- chreekat: let's remember for mvp there's only 1 project, and people can simply unpledge one project
