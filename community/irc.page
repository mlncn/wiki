---
title: #snowdrift IRC Channel
categories: community 
...

We have a general live chat channel on **freenode.net** at **\#snowdrift**.

## IRC clients

You can access our channel with any IRC client. For dedicated client apps, most GNU/Linux systems offer built-in options such as Konversation for KDE or Empathy for GNOME, otherwise see this [list of FLO IRC clients](https://prism-break.org/en/protocols/irc/)

For web clients, one option is the [KiwiIRC web interface](https://kiwiirc.com/client/irc.freenode.net/#snowdrift). The official Freenode.net [web IRC interface](http://webchat.freenode.net/?channels=#snowdrift) works too, but isn't as nice.

Another option is to use the bridged room through Matrix.org (a newer protocol, similar to IRC), such as with the [Riot client](https://riot.im). The room address in Matrix is `#snowdrift:matrix.org`.

## Chat history

The Matrix.org clients allow scrolling back in time. Otherwise, the IRC channel is logged at [ircbrowse.net/browse/snowdrift](http://ircbrowse.net/browse/snowdrift) (click "last" to see the most recent activity).

## IRC usage

While our [email lists](https://lists.snowdrift.coop/) are good for longer posts and specific topical discussions, IRC is more loose and casual. IRC discussions are often real-time or close and have a flat, ongoing format instead of threaded topics. As a community meeting place, we chat about various ideas, ask questions, socialize, and so on — anything connected to Snowdrift.coop (even just tangential).

### IRC bot

We have a bot ("fpbot") on the channel which will one day handle alerts, integration with the website, and more. For example, you can leave a message for another member with the following command: `fpbot: tell username Your message`.

### Salticons

You might see us using some emoticons you don't recognize. They're called Salticons, because [Salt](/community/team) (one of our team members) came up with them.

Salticon | meaning
---------|------------
d               | thumbs up
o/              | waving, or raising a hand
\\o              | waving in the other direction \
                     (usually, waving goodbye)
\\o/            | hooray!
o\\              | facepalm
/o\\            | head in hands\
                    (or double facepalm)


### Rules

Like other discussion, chat in the IRC channel should follow our [Code of Conduct](conduct). Note that flooding the channel with overwhelming quantity of messages will be considered spamming regardless of the content.

As we cannot flag and edit specific comments in IRC, please send a polite, *private* message to anyone violating the Code of Conduct asking them to improve the issue going forward.

If someone does not show willingness to address friendly complaints or if they continually post things in violation of the Code of Conduct, alert a moderator.

Please also consider the [freenode.net channel guidelines](http://freenode.net/channel_guidelines.shtml) and the rest of the freenode.net policies.
