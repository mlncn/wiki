---
title: Volunteer and Project Team Engagement 
categories: community
...

## Seek consensus first

We suggest projects use consensus as a first-line approach to decision-making. By applying consensus to project decisions, you can ensure that team members, contributors, and patrons alike remain on board with the project direction. See our facilitation page in the [Governance](/governance) section for more details about how to use a facilitated consensus process.

## How to recruit and keep volunteers

a. Welcome volunteers, make it clear that participation is open to all
    * Introduce people **thoroughly**: make sure new volunteers know enough about the people they will work with to carry on a conversation with them (role, interests both inside and outside the project, bio, etc) and vice versa, this goes double for new project team members
b. Provide resources for volunteers to learn about the project in depth
    * Have a system to coach them through the learning process, ideally with real human guides along with the following documentation:
        * How to install / how to set up anything technical needed to work with the project
        * How to create and submit changes or new additions
        * Project roadmap
        * Specific issue tickets
        * Change log
        * Glossary
        * Where to get help
c. Provide clear, easy first tasks in order to develop volunteers' self-confidence and skills and build from there
    * Leave some easy tasks for volunteers
    * Suggest that volunteers write new tickets based on their own suggestions and feedback; but make it easy to identify existing tickets to avoid duplication
    * Make sure volunteers feel okay if they drop one or two assignments, don't let a failure turn someone off the entire project
d. Give thanks and encouragement
    * Be prompt with implementation of changes so volunteers can see the concrete results of their work
    * Share thorough and sincere thanks for every contribution
    * Have a larger narrative (preferably with a real person as an example) in which volunteers can place themselves so they see opportunity for recognition and advancement
    * Snowdrift.coop will have a "thanks" button so project teams can award volunteers a "contributor" badge for the project
    * Reward volunteers who go above and beyond with official titles or positions on the project team (even if they continue only as volunteers)
        * Full membership in a project team should not depend on paid status, — a volunteer member could hold as much or more authority than a paid member depending on their role

## Dealing with burn-out

* Consider finding partners for volunteers so they can pair up to work together and support one another
    * Work to have at least two people knowledgeable in all specialized subject areas, so that no process depends solely on one person's availability
    * Have new volunteers learn by shadowing others to reduce strain on the trainer
* Check in regularly to see how people are feeling and show sensitivity and support
* Make plans for rotating unpopular or especially stressful jobs
    * Rotation can also be effective at alleviating boredom and bringing fresh perspectives
* When scheduling, plan for team members needing personal days
* Have a general plan for how to transition people out of the project, should they need to withdraw
    * This should include exit interviews (reasons for leaving, whether there was anything that could have induced them to stay, what they liked about the project, whether they would recommend it to others)

## Effective meetings

Organizations need some meetings, and when run well, they can produce powerful results. But don't hold meetings for the wrong reasons. Avoid formal meetings when you have no clear agenda or issue to address or if more general chat or email exchanges could adequately solve the problems.

**Good reasons to hold a meeting include:**

* To solve problems which will benefit from input and buy-in from the entire group
* To communicate between teams (especially those that do not have a designated liaison)
* To make sure important information is clarified and uniformly understood
* To set major priorities, especially for the long term
* To conduct project post-mortems in order to reflect on what succeeded and what needs improvement

## Starting a project

* Be social.
    * Build your team and community from the beginning.
    * Talk to your friends about your project. Talk to their friends about it. Talk about it in public IRC channels, forums, and so on.
    * It's okay if it's not "ready" yet -- just get the word out and find interested people as early as possible.
* Make it a community effort. Rather than *your* project, aim to make it *our* project. Let your new contributors have a say in the direction.
* Ask for help. Even if a small number of people do the real work, advisors can help in finding direction and making decisions.
* If you can find a good fit, work with a co-founder.
* Open it out of your circle. Seek ways to appeal to the general public rather than strictly aiming for the smaller set of enthusiasts already interested in the project's topic.

## Resources

* Team-building tools and organizations
    * [OpenHatch](http://openhatch.org) helps new volunteers get involved in FLO software development
* Additional reading
    * [Guide to effective mentorship in FLO software](https://binaryredneck.net/2017/06/04/getting-mentorship-right-oss/) by Susan Sons