Snowdrift Alpha Sprint:  Notes on what still needs to be fixed/implemented

Wiki for possible use in organizing the sprint: https://snowdrift.coop/p/snowdrift/w/en/alpha-sprint

Note for wolftune: /auth/logout is included in the list of necessary pages, even if it isn't a visible page. (Shouldn't return 404 etc.)
Q for chreekat: do we need to list pages that existed from before and that need no changes? Isn't /auth/logout that? Isn't listing that just more clutter?
A: The list of pages is the blueprint for the sprint.

mray says chreekat might have work mray did but isn't merged into master and should be, let's clarify that


THINGS TO CHECK IF GOOD AFTER ALPHA BASICALLY DONE
	* Add default styling for form elements (e.g. https://snowdrift.coop/auth/login and https://snowdrift.coop/u/1/edit )
	* Link styling (visited/unvisited)
	* responsiveness for alerts (probably Eunice on top instead of side for small screens) [Robert did, waiting for  (done in MR-133)
	* Sponsors link move to within partners footer, not general footer (not alpha essential)
	* move notifications/archived to new framework just like /notifications (done in MR-135)
		* these routes aren't good, but fixing them is for post-alpha, SD-614
	* need some standard CSS something for lists ul, ol, li etc (e.g. https://snowdrift.coop/trademarks )
	* BUG: POST method missing from project, can't edit via /p/handle/edit (well, can't hit "preview" to submit the edits), so that different projects have different logos and descriptions

soon after ALPHA (delay for now)
working search
dashboard for project teams
project editing
	* /reset-password: apply new styling and add Mimi/Eunice trying-to-remember style illustration (Robert says not priority)
	* /honor-pledge: apply new style and add illustration (not priority for now)



High Level To-do Items
	* Solidify nesting / wrappers coding
		* How to have wrapper functions for things like /p/ that don't necessarily force every /p/ URL to use them, but when we do use them for a given page, then a desired set of templates are included as a wrapper around any additional appropriate templates; perhaps e.g. /p/patrons would be called with `defaultProjectLayout "patrons"` which would know to use project.hamlet and project.cassius, apply .project class to everything in its scope, and then within that call the project/patrons hamlet and cassius files and include a .patrons class for the stuff in that scope?
		* tests and documentation about how to manage this sort of nesting/wrapping reliably and consistently
	* Complete all tasks for the pages listed below.
		* Sketch out the whole UI
	* Shirts
	* Stickers
	* Test framework for site elements
	* Test framework for mechanism
	* Workflow for managing projects
	* Have some number of real projects to choose from
	* Engage advisors by asking specific questions about our plan
	* Have a plan for distributing/promoting the site

Page-specific tasks
	* /create-account: remove extra non-required profile stuff that can be filled in later
	* /notifications:
		* clean up Hamlet/Cassius for just decent layout, spacing
		* incorporate into dashboard wrapper
	* /notifications related (navbar really): bring back notifications indicator with number in navbar
	* User dashboard (or maybe navbar): add some way to access log-out on small screen
	* (...this list should be completed by pulling tasks out of the following list)


Scratchpad List Of Page-Specific Tasks
	1. User dashboard (like https://git.gnu.io/settings )
		2. Pledges /pledges
			3. just a stub for now explaining it will be a place to manage pledges and see stats or something
		2. Project roles  /roles
			3. not sure we need this in alpha actually if we instead just keep the roles listed in the user profile
		2. Edit profile /u/#/edit
	1. About Us (follow github.com/about as a model somewhat)
		2. /about
			3. General brief statement about the project and concept, links to other /about pages
			3. Should actually keep most of the old-homepage *content* (what is currently the about stub), including link to more thorough writings on wiki pages and video from fund-drive
			3. restyle to new design
		2. /about/team
			3. Largely adaptation of current /p/#ProjectHandle/who but more curated and careful
			3. Describe core team, active volunteers, generic thank you to other volunteers, section for advisors, section or link for 
			3. Write in third person?
			3. Avatar / image for each main entry
			3. Section honoring past team and advisors
		2. /about/press
			3. See current press wiki page for reference, but build new better design
			3. Main content is "Press Highlights" with links to articles etc
			3. For info-for-press mention or include contact links, link to logo resources to use, link to terminology wiki page, maybe to blog (for news sources, GitHub does that)
				4. /assets (like http://maidsafe.net/assets )
			3. include presentations and podcasts? (see presentations wiki page)
		2. /about/contact
			3. Aaron says: should this maybe be /contact instead? That's how GitHub does it
			3. This should be a general list/guide of the different ways to reach us for different purposes, some of which link to /p/snowdrift/contact
				4. email
				4. IRC (ideally link to embedded client at /p/snowdrift/contact as described below under Project pages
				4. GNU Social / twitter
				4. brief apology for why not Facebook etc?
				4. link to /p/snowdrift/d
				4. email lists
				4. maybe link to /p/snowdrift/volunteer?
				4. link to git.gnu.io stuff?
	1. How-it-works Pages
		2.  /intro
				4. close to acceptable as is, needs CSS
		2. FLO /how-it-works/flo
			3. Need text and illustrations discussing intro to FLO values, then link to wiki writings for further reading
		2. Network Effect /how-it-works/network-effect
			3. Brief elevator-pitch explanation of snowdrift dilemma
				4. illustrate the blocked road, the ideal cleared road, the toll-road full of billboards if we can't cooperate
			3. Explain the mechanism
				4. basic pledge, a couple accessible graphs
				4. emphasize control over system-wide budget
				4. emphasize that this is new and untested but we've considered allt the potential concerns, link to wiki for more reading
		2. Sustainable funding /how-it-works/sustainable-funding
			3. emphasize ongoing projects, livable salaries, accountability
		2. Co-Op /how-it-works/co-op
			3. Describe basics of co-op
			3. emphasize democracy and ethics, empowerment
			3. emphasize .coop as sponsored domain
			3. our three member classes
			3. mention work-in-progress
			3. link to wiki for more reading
	1. Project Signup /project-signup
		2. clear info about the roll-out plans and info to just get in touch with us if interested in participating pre-beta
		2. Signup Form (not listed) /project-signup-form (maybe NOT for alpha)
	1. Project List /p
	1. Project "home page" /p/#ProjectHandle/.... (note: consider iframes to embed old pages within this)
		2. Updates .../updates
			3. Basically a curated highlighted set of feed items
			3. possibly add more new parts, but probably just *link* to /feed because it includes wiki and discuss events and we won't have those moved over yet; include watch-project button
		2. Wiki (links to existing wiki for now)
		2. Discussion (links to existing)
		2. Transactions .../transactions
		2. Edit project /p/#ProjectHandle/edit
		2. Contact ../contact
			3. existing contact form, links to mailing lists, links to social-media etc, embedded IRC client (Robert's idea, SD-685)
	1. Policy pages (all only need CSS for lists to be implemented, otherwise alpha ready enough)
		2. /terms  (Terms Of Use)
		2. /privacy  (Privacy Page)
		2. /trademarks
	1. User Profile page (public) /u/3  (like https://git.gnu.io/u/chreekat )
		2. Needs mockup, redesign, specs
		2. How relates to dashboard? (old user profile has dashboard items linked there, visible to user themselves)
	1. Search page (not functional though) /search
		2. DONE given staying non-functional
	1. Donate /donate
		2. general design, CSS cleanup
		2. link to new /merchandise to show stickers/shirts
		2. maybe Mimi/Eunice illustration
	1. Sponsors /sponsors
		2. centered headings? mosly DONE
	1. FAQ (for alpha sprint, link to to wiki page initially, at least prioritize everything else)
	1. error pages (e.g. 404 and permission-denied)
		2. just needs styling, ideally Mimi/Eunice illustration
	1. JS Licenses /js-licenses
		2. DONE
		2. Bonus: would be nice to link to
			3. https://www.gnu.org/philosophy/javascript-trap.html
			3. and maybe https://www.gnu.org/software/librejs/
			3. and maybe https://www.gnu.org/licenses/javascript-labels.html






REFERENCE MATERIAL




Master List of Pages to Complete for Alpha
	1. Welcome page (/welcome, and "/" for non-logged-in users)
	1. Auth...
		2. Login /auth/login
		2. Password reset /reset-password
		2. Logout /auth/logout
		2. Create account /create-account
			3. Honor pledge /honor-pledge
	1. User dashboard (like https://git.gnu.io/settings )
		2. Overview (/dashboard and "/")
		2. Transactions /transactions
		2. Notifications /notifications
		2. Pledges /pledges
		2. Project roles  /roles
		2. Edit profile /u/#/edit
	1. About Us /about
		2. /about/team
		2. /about/press
		2. /about/contact
	1. Assets /assets
	1. Introduction Page /intro
		2. FLO /intro/flo
		2. Network Effect /intro/network-effect
		2. Sustainable funding /intro/sustainable-funding
		2. Co-Op /intro/co-op
	1. Project Signup /project-signup
		2. Signup Form (not listed) /project-signup-form
	1. Project List /p
	1. Project "home page" /p/#ProjectHandle/....
		2. Updates .../updates
		2. Wiki (links to existing wiki for now)
		2. Discussion (links to existing)
		2. Transactions .../transactions
		2. Edit project /p/#ProjectHandle/edit
	1. /terms  (Terms Of Use)
	1. /privacy  (Privacy Page)
	1. /trademarks
	1. User Profile page (public) /u/3  (like https://git.gnu.io/u/chreekat )
	1. Search page (not functional though) /search
	1. Donate /donate
	1. Merchandise /merchandise
	1. Sponsors /sponsors
	1. Blog (Aaron says: this should be a sub-item under 8)
	1. FAQ (for alpha sprint, link to to wiki page initially, at least prioritize everything else)
	1. error page (e.g. 404)
	1. JS Licenses /js-license
