---
title: Next Steps for Snowdrift.coop
categories: project-management
...

The Snowdrift.coop [funding mechanism](/about/mechanism) is the central feature among the many ambitious goals involved in building a holistic system supporting our [mission](/about/mission). This page describes broadly the necessary next steps to achieving a functional funding system.

## Self-funding as MVP

The first major milestone we aim to reach is *funding Snowdrift.coop through the crowdmatching mechanism*. Having one project with pledges and funding operating effectively will provide the foundational "MVP" (Minimum Viable Product) upon which we can build the rest of the platform and then invite other projects to join.

## Next steps

### Launch the MVP

This will be a "semi-open" beta.

- Implement the accounting and bookkeeping needed by the funding mechanism (in progress)
- Contact users who need to create or verify their email addresses.
- Do the switchover.
    - Back up wiki discussions before taking it offline (#366).
    - Migrate user auth.
    - Update master branch to deploy to production.

For specific details, check out our project management system on [Taiga.io](https://tree.taiga.io/project/snowdrift).

## Tasks for upcoming milestones

* [Too many to list here]

Once we launch the MVP we will need to take stock of open tasks, recurring tasks, possible directions, and likely paths to success or failure. There is a lot to do.

### A longer list for the bold

* Implement user dashboard and project pages that present pledging UI and show patron status (design prototype exists).
* Implement limits, pledge suspension, and rollover payments.
* Implement procedures and tools for backups and security.
* Support multiple projects on the crowdmatching platform!
* Organizing the wiki content to provide answers to all common questions.
* Produce illustrations about the concepts and system, ideally videos (see the [Design](/design) page for more details).

#### Other ongoing tasks

* Follow-up on our [fund-drive](https://snowdrift.tilt.com), finish sending out rewards to the donors.
* Continue organizing our volunteer recruitment, management, delegation, and related tools etc. See [How to Help](/community/how-to-help).
* Solidify our team organizing and workflow management.
* Participate in conferences.
* Pursue [partnerships](/community/partners).
* Work on [Internationalization](international) / [translation](/planning/translation).

#### Legal issues

* Have Terms of Use, Privacy Policy, and financial transaction rules reviewed by a lawyer.
* Have Bylaws reviewed by a lawyer, then appoint board and approve bylaws which will be ratified at the first member meeting.

We also have a [legal](/legal) page with more details.