---
title: Videos for Fund Drive
categories: communications
...

Update: Our intro video is at <https://archive.org/details/snowdrift-fund-drive-intro>

## Suggested guidelines

* Recommended length of 4 minutes or less (ideally, 2 minutes or less with attention-grabber within the first 5-15 seconds) for main campaign video.
    * Additional videos can be included in the detailed project description.
* Need to make sure video and all other materials are easily shareable through social media for maximum impact; this is especially an issue if we self-host.
* Updates during the project campaign are essential to avoid a mid-campaign slump between exciting launch and finish line periods.
    * Functioning blog and a way to send out e-mail announcements are important elements.
* Developing visually interesting presentation in general needs to be a priority, both for the video and for the written project description.

## Elements

Here are some elements to consider. There may be others to add. We can obviously use ideas from the writings across the site.

### Personal story

We should have some time with David and Aaron each saying something about their story and motivations.

See [Aaron's profile](https://snowdrift.coop/u/3) for his personal story, much of which has illustrations available.

### Intro to problem

Laying out the context, what we're trying to solve.

Reference the shittiness of ads and surveillance and how economically independent projects can forgo that crap. Consider the nice presentation ideas from Aral Balkan and the [Indie Tech Manifesto](http://ind.ie/), such as "Trickle Down Technology" — we can't get real value for the public interest just by technologists serving their own interests.

Also: the public is the *client*. It's just we need a critical mass of people to be viable. And FLO in part because you don't give the client (the public) a half-assed restricted license. We, the public, are contracting with projects to get true public goods that serve the public interest.

#### Patronage allegory?

Olden days patronage system; then copyright enables more market-based approach; the funding part of copyright is positive but the exclusionary and power/control parts stink. We offer a return to patronage while *incorporating* the benefits of a bottom-up market; the *community* becomes the patron instead of the aristocracy.

### Mechanism

We need a super-concise, well-illustrated description of how the system works.

Reference with an image, the button showing the idea of
"1,285 people are supporting this project, and they've pledged to donate MORE if you'll join them"

#### The pledge mechanism

The precise process of how projects get funded, and address the doubts that come up (I'll be charged an infinite amount? There's no cap? No, no, you just make available a limited amount to the system, like with Flattr).

#### Accountability

We should make brief reference to autonomy of projects balanced by systems for feedback and accountability.

### Endorsements

Ideally get some clips from outside endorsers talking about why this matters and why this is the right project

### Funding needs

Describe more why we need funding, where we're at currently, etc.

### Concrete examples

We should reference real-world examples so it isn't hypothetical. KXStudio is made by guy in Portugal struggling and poor. Task Coach is developed by a couple folks in their spare time even as it is better and more responsive to the users than proprietary software that gets $millions… We should provide some hard numbers on funding and users and more. Ideally not use just software examples, but also educational resources, research, art, journalism…

### Running as a co-op

Maybe in a separate video, but we should describe how we work as a co-op.

Illustration idea: show how people see things from different perspectives (for example the "perception is creative" bit from Nina's [Cult of Originality](http://blog.ninapaley.com/2009/12/28/the-cult-of-originality/)), and use that to reference the value of the *solidarity / multistakeholder* approach.

Also, our corporate system consolidates power on the supply side while keeping consumers as separated and independent as possible. That imbalance causes many problems. Breaking up monopolies and oligopolies is one thing, but not a challenge we are up to. So to balance the power, we're providing a means for the demand-side to better coordinate our power. It's not an us vs them issue though. All sides have a stake in the co-op as well, as we can all work together for win-win solutions.

