---
title: Advisors 
...

# Welcome to the Steering Committee

We're so glad to have you on board!

We know you are busy. We all have many commitments. Thank you for making this important project enough of a priority.
We believe that Snowdrift.coop has the capacity to support valuable projects and empower people around the world.

## Committee mission

The committee serves as a guide and facilitates decision-making as we develop our policies, technical tools, and presentation. We want to provide the best foundation to then hand over to the initial board of directors.

## Steps for new committee members:

* revisit the [join the committee page](joincommittee) to review the committee responsibilities
* click your user account listing at the top of the sidebar (if you used Persona when creating your account, your account is your e-mail but is presented publicly as just a user#)
    * Edit your profile to add your name and some avatar. This is used in site discussions and on [/who](/p/snowdrift/who).
    * Adding a brief blurb mentioning who you are, perhaps links to external websites or anything else you want listed in a single paragraph
    * Optionally: write a longer statement or narrative about yourself or your feelings and wishes for the project etc.
* click "discuss this page" here at the committee page and introduce yourself to the rest of the committee with a new comment.
* If you haven't already, read through all the **[About](about)** and **[Next Steps](next)** pages, including the various links to specific internal pages.
    * As you read, feel free to do minor edits of wiki pages and/or add discussion comments as you see fit.

## Notes about the site

*Below are other items committee members should know to make your way around the site:*

* Any page with /w/ in the URL is a [wiki](wiki) page.
    * each project is listed at snowdrift.coop/p/*project* and then wiki pages build on that, so each project has its own pages
    * [/w](/w) on its own lists all the wiki pages and their permissions

* All pages have permission settings
    * by default, a regular page allows logged-in users to comment, but these submissions are moderated until users are marked "established" after we've verified that they are not spammers or trolls
    * a *public* page is one where even anonymous visitors can submit *moderated* edits or comments
    * a *moderated* page makes all submissions moderated so that editing is limited to a more strict set of users with special permissions, but anyone can still submit moderated postings.

* [/w/joincommittee](joincommittee) presents info for committee candidates, whereas you are looking at [/w/committee](committee).
* [/invite](p/snowdrift/invite) after a project offers a way to make an invite code for someone to add a special role.

## Off-site resources

### [IRC](irc)
