---
title: Illustration Ideas
categories: communications
...

>Note: these are general ideas only. They have not yet been confirmed for
>implementation.

An excerpt of ideas added by wolftune:

- Basic [economic](https://wiki.snowdrift.coop/about/economics) terminology
    - Rival vs. non-rival goods / scarcity vs. post-scarcity economics
        - Ideas vs containers (see
          <http://questioncopyright.org/how_to_free_your_work>)
        - Inappropriate piracy metaphor: traditional pirates ransack ships and
          take things; but *copy pirates* come up to a ship and then *BLOOP*
          have duplicate cargo! No loss of original object, no piracy. Value of
          original object has simply changed because of scarcity status.

- [Mechanism](https://wiki.snowdrift.coop/about/mechanism) illustrations
    - Cartoon guy going through the steps of using the system, adding some $,
      make some pledges, gets matched, funds run out, adds more, etc.
    - Could contrast our system and the bigger difference from matching versus
      plain donation
    - Network effect: technical term for our matching system. When every node
      in a network can connect to every other node, network utility scales
      quadratically while the number of nodes scales linearly, as each new node
      may connect separately to each old node. Wikipedia public domain
      [illustration](http://en.wikipedia.org/wiki/File:Network_effect.png)

- [Game theory dilemmas](https://wiki.snowdrift.coop/about/snowdrift-dilemma)
    - Prisoner's Dilemma: matrix illustration of the four possible outcomes,
      showing why defection is the best default strategy
    - Snowdrift Dilemma: the same, this time showing why default strategies
      vary between defection and cooperation
    - Iteration: showing that information carries over from one cycle to
      another (remembering past slights or assists)

- [Slogan](https://wiki.snowdrift.coop/communications/slogan) and
  [presentation](https://wiki.snowdrift.coop/about/presentations)
  ideas/concepts are always open for illustration

- Infographics for each area of our [targeted
  messaging](https://wiki.snowdrift.coop/about/targeted-messaging)
    - Music idea: participatory vs performative: music played to exclusive paid
      audience behind a wall by professional performers vs group music making.
      We invite the professional music to be shared more freely and the pros to
      come join in, lead, coach, fully participate with the groups of folk
      musicians; thus keeping everything participatory but raising the quality
      and still providing livelihood for the pros.

- Possibilities beyond existing pages
    - Narrative stories about who we are or why we're involved with
      Snowdrift.coop
