---
title: Our Use of Mimi & Eunice
categories: branding, design
...

In various places, Snowdrift.coop uses the characters "Mimi & Eunice" which we adapted from art by [Nina Paley](https://en.wikipedia.org/wiki/Nina_Paley).[^nina-politics]

<div style="text-align: center;">
![](/assets/sticker-handshake.png)
</div>

Paley published the original comics from 2010 to 2012 while also serving as Artist-in-Residence at [QuestionCopyright.org](https://questioncopyright.org) (a partner of ours) where she produced free culture works such as their ["Minute Meme" videos](https://questioncopyright.org/minute_memes) and the booklet, [*Mimi & Eunice's Intellectual Pooperty*](https://ia600300.us.archive.org/1/items/MimiEunicesIntellectualPoopertyMinibook/IP_Minibook_Interior.pdf).

We initially used select comics to illustrate our early writings. Later, we adapted the characters ourselves to illustrate the [snowdrift dilemma](/about/snowdrift-dilemma) and more.

[^nina-politics]: Paley has long expressed provocative political views through her art. For example, prior to her focus on anti-copyright and free-culture advocacy, she produced comics for the Voluntary Human Extinction Movement which opposes human reproduction. More recently, she has taken controversial positions on gender-identity politics.

    Other than the specific free-culture comics and illustrations we use, we do not endorse any of Paley's other views. Likewise, Paley does not specifically endorse any particular use we make of her art (although she is aware of our use, and we credit her as required by the CC BY-SA license).

    As an organization, Snowdrift.coop's [Values and Principles](/community/values) describe our support for free/libre/open ideals, cooperative ethics, and celebration of diversity. We do not otherwise take stances on specific political issues.