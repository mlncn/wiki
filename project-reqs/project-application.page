---
title: Project Application
categories: governance, project-liaison
...

## So You'd Like to Fund Your Project Through Snowdrift.coop…

You've got a FLO project that meets our [baseline requirements](/project-reqs) and you've decided to sign up. Great! Here's a guide to the application process:

## Project application

1. Consider the following points:
    * You need defined goals involving non-rival products that funding from Snowdrift.coop will help support.
        * Software projects produce usable programs, artistic or journalistic projects produce writings or other media; a research project will have questions and a plan for how to answer them and publishing the results; and so on.
        * Given our continued monthly support model, we emphasize on-going projects. It helps to have a clear plan for continued development, additional creative output, a commitment to follow-up research, etc.
    * Have a reasonable estimate of what resources (human and otherwise) you will need to accomplish your goals.
        * Give potential patrons an idea of what work their support will fund. What size project team? Are those people already on board or do you need to recruit them? Do you need to make monthly minimum so that someone can leave a day job in order to focus on the project? If you can survive on volunteer effort alone, do you need monthly funding for other basic costs?
    * Gauge potential interest and evaluate the existing market.
        * Snowdrift.coop cannot support every possible project. Our matching algorithm actually helps to eventually winnow out projects that lack adequate support. We cannot guarantee your success.
        * Consider whether you could achieve your goals by assisting existing projects instead of creating a new one.
    * During our beta stage, we are only accepting established projects that have already formed a project team, garnered community interest, and shown progress towards their stated goals. Once the site fully launches, we will allow start-up projects to also apply.

2. Consider the [project requirements](/project-reqs).
    * Understand that the Free/Libre/Open definitions we follow mean that your license cannot discriminate against commercial uses (see [problems with non-commercial restrictions](whyfree#problems-with-non-commercial-licenses)). If you are concerned about protecting your work from proprietary co-option or exploitation, simply choose a copyleft license. See our license discussion in the [About](/about) section for more information.
    * Read the [honor code for projects](honor-projects).
        * On the application form, you will indicate your status on each area identified in the honor code. We do not require that you meet every ideal but simply that you report your status honestly.
        * You will have the chance to provide explanations about particular divergence the honor code (for examples: your research project cannot publicly release its source data because of privacy concerns for study participants; or you currently use just a Facebook group instead of an independent mailing list but you would accept help in transitioning)
        * Your project profile will include your report so that patrons can make informed ethical choices about their pledges.
    * Be prepared to implement our [transparency requirements](transparency).
        * Keep a record of all formal project participants, even those who are not Snowdrift.coop users.
        * Have an established decision-making process and communicate in public channels as much as possible.
        * Know your upstream sources and give them credit.
        * Have clear and detailed accounting records of income and expenditures from **all** sources. You must publish general budget summaries in some form once you are approved for Snowdrift.coop funding.
        * Make sure your budgeting and accounting systems can handle our requirements for use of funds. If you are a for-profit enterprise or you also produce rivalrous goods, you must be able to show that funds raised through Snowdrift.coop are being put to appropriate development use and not directly toward profit.
    * Have a plan for the format and repository (see [Market Research](/market-research) for a list of resources) you will use to release your works.

3. Submit an application. ***Note: this process for handling applications is planned but not fully in place as of August 2015***
    * Designate a project member (preferably a team leader) who will serve as Snowdrift.coop's primary contact during the application process.
    * Create an account on the site if you have not done so already. Also, we will need emails for all formal team members and real names and mailing addresses for any team member that receives project funds. We recommend that all team members register in advance and fill out the email and user bio sections in their account information. This will expedite the application process.
    * [Contact us](https://snowdrift.coop/contact) or come chat on [IRC](/community) if any questions arise while filling out the application.
    * Once your application is submitted, it will be reviewed by an approval team (which will hopefully include at least one expert on the particular type of project involved). The approval team's job will be to determine whether the project is viable and consistent with Snowdrift.coop's mechanism and values. They will ask any follow-up questions necessary to clarify the application before making this determination. If they identify concerns, the team will work with you to resolve them. If there are any fundamental issues that cannot be solved, your project will not get listed. Otherwise, welcome to Snowdrift.coop!
    * Once your project is accepted, you can customize your project's listing page, link your own site to Snowdrift.coop, and start recruiting patrons!
