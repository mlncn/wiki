---
title: Snowdrift.coop Board of Directors Standing Rules - Working Draft
categories: governance
...

## Membership Agreement

A basic legal agreement between the co-op and the individual member should cover, among other things:

* Member rights and responsibilities
* Consent to the dispute resolution/termination procedures
* Liability limitations
* Privacy disclosure statement for any collected information, especially any that will be made publicly available.
    * For example, do we need to record full legal names and will those names and other contact information be available to other co-op members seeking petition signatures?

The membership policy is to be reviewed in full for necessary updates once per calendar year. As it is subject to contract law, any changes should be examined by legal counsel to determine whether the changes should apply to all co-op members and whether current members will need to sign an addendum to their original membership agreement.

## Voting

* When a petition prompts a special election for a Board position, should the regular nomination procedures still apply (which means they must fully run their course before the election may be held)?
    * Should there be a compressed nomination procedure for special elections to guarantee timely execution?
    * Board nomination procedures should always encourage diversity of backgrounds and viewpoints, even when nominations must take place in a compressed time frame.
* Members are encouraged but not required to discuss issues with all affected stakeholders prior to initiating a petition drive. 
    * After a petition has started, may it be amended if all signers consent? 
    * May petitions be pulled by the original petitioner? Does this include after they have reached their signature threshold?
* Special election ballots shall be drawn up by a six-member committee appointed by the Board and tasked with addressing any legal concerns, wording the ballot question(s) for maximal clarity, and considering all stakeholders and views on the issue. The committee must include the original petitioner(s) and follow the same consensus/fallback procedure as in Board decisions. In addition to the appointed members, any signatory of the original member petition(s) may also join the ballot committee.
    * Does the petitioner's absence prevent the committee from acting, or negate their right to representation on the committee, or cause the special election to be dropped altogether?

## Meeting Notice and Attendance

* A member always has the right to waive any notice required by applicable law, Standing Rules, or the Bylaws. The waiver must be in writing, bear the member's manual or electronic signature, and be delivered to the co-op for inclusion in the minutes. 
* A member’s attendance at a meeting  waives objection to (a) lack of notice or defective notice of the meeting, unless the member presents an objection at the beginning of the meeting to holding or transacting business at the meeting, and (b) consideration of a particular matter at the meeting that is not within the purposes described in the meeting notice, unless the member objects to considering the matter when it is first presented.

## Board of Directors Operations

* Board members have the right to request new facilitator at any time should they believe a conflict of interest is present and if another facilitator is readily available. If a substitute is not readily available, the Board should seriously consider postponing the business at hand until another may be found, unless the issue under consideration is time-critical and cannot be delayed.
* The Board may put any issue where full member input is deemed necessary up for advisory or binding membership referendum.

### Meeting process

* A program or agenda listing all business items shall be prepared in advance of each non-emergency meeting by the Board President and sent to Board members at least 7 days before the meeting date.
* Meetings shall run by best practice guidelines for [facilitated consensus problem-solving](facilitation)

### Order of business for regular Board meetings

* Convene within 15 minutes of published start time whether all members have arrived or not.
* Roll call.
* Review preceding meeting's minutes.
* Committee reports.
* Officer and GM reports.
* Old and unfinished business requiring action.
* New business requiring action.
* Informational or educational agenda items not requiring action.
* Adjournments.

### Asynchronous online forums

Collaborative document or policy work may take place in an authenticatable asynchronous online forum provided that all required members of the Board or the relevant Board committee agree on the medium and the time-frame for such collaborations. It is strongly recommended that the medium and time-frame be agreed upon in advance through a synchronous communication medium before asynchronous work begins. Further, it is strongly recommended that the Board or committee use the most public forum appropriate to the work at hand. For example, the public project wiki is most appropriate for work ready for general membership scrutiny, while a Board members-only Etherpad might be appropriate for a first draft proposal.

### Annual financial audit

* The board of directors shall engage the services of a Certified Public Accountant / Public Accountant for the purpose of auditing the financial records and accounts of the cooperative at the end of each fiscal year.
* The engaged accountant shall be instructed to prepare an Unqualified Opinion / Qualified Opinion audit of the financial records. The final prepared audit shall include, but not be limited to: a balance sheet, statement of operations, source and use of funds, and the necessary supplementary schedules.
* Upon completion of the audit, a full report shall be presented to a quorum of the Directors at a Board meeting. The accountant’s reports must be approved or rejected by official Board action, included in the Board's public minutes, and published on the project website for member reference.

### Special Board meetings

* The President or a majority of the Board of Directors may call a special Board meeting at any time in response to emergent issues that cannot be adequately handled within the regular meeting framework.
* Board meetings held at non-standard times / places / mediums but arranged through normal meeting procedures are regular meetings, not special meetings, and are not subject to the Standing Rules in this section.
* Each call for a special meeting shall be in writing, shall be signed by the person(s) calling the meeting, shall be addressed and delivered to the secretary, shall state the time / place / medium for the special meeting, and shall specify the subject to be discussed.
* Only the business specified in the written notice shall be transacted at a special meeting.

## Employment

### Classification of Employees

All employees will belong to one of the following classifications:

* Permanent full-time: An employee who works a minimum of 32 hours per week, 39 weeks per year, and is entitled to full participation in benefit programs.
* Permanent part-time: An employee who works a minimum of 10 hours per week, 39 weeks per year, and is entitled to partial participation in benefit programs.
* Temporary: All other employees, who may be entitled to partial participation in benefit programs based on conditions of their employment contract.
    * A temporary employee who is later classified as a regular full-time employee will be entitled to benefits from the time of last initial employment.

### Employee performance appraisal

Formal appraisals of each employee’s performance will be conducted at least annually, although supervisors are strongly encouraged to provide performance feedback and opportunities for employee feedback throughout the year.

The evaluation will be based on each employee's position description and performance standards. Description and standards will be established for each position as it is created and should be evaluated annually in response to employee and supervisor feedback, as well as whenever there is a substantive change in the work or working conditions.

Basic performance standards for all employees:

* Active participation with one's supervisor in the appraisal process.
* Attendance at all required employee meetings and trainings.
* Upholding the good name of the cooperative (in exceptional circumstances, this may include certain off-duty conduct).
* Conducting oneself in accordance with the cooperative values of equity, responsibility, and solidarity.


### Counseling

Management maintains at all times an “open door” employee counseling policy for individual employment concerns, including educational and career development, relationships with fellow employees, and other work related issues.

### Nepotism

* No Board member or employee shall participate in evaluating a relative or spouse's fitness for potential employment.
* All immediate family relationships must be fully disclosed during the hiring process, as well as any prior business dealings or other conflicts of interest.
* Deliberate concealment of a conflict of interest is grounds for immediate termination.

### Pay periods for employees

* All regular salaried and hourly employees shall be paid on a biweekly basis.
* Independent contractor work agreements may specify another payment schedule, or may specify lump-sum payment.
* Hourly employees shall be entitled to a time-and-a-half pay rate for overtime if management assigns more than 40 hours of work in one week or more than 10 hours in one day or if assigned to work on a staff holiday.
* The cooperative shall recognize the primary holy days of an employee's religion as personal staff holidays, and the Board shall be empowered to designate general staff holidays for cultural celebrations such as International Workers Day or Thanksgiving.

### Personnel Records

* A confidential individual personnel file shall be maintained for each current employee. This file shall contain such information as the employee’s application form, references, State and Federal withholding tax forms, yearly performance appraisal reviews, and other employment history.
* A confidential inactive file shall be maintained for at least 3 years for persons who have left employment of the cooperative. After this period, a record indicating dates of employment and job titles held shall be maintained.

### Co-op membership requirement

* As all Snowdrift.coop team members are automatically co-op members, they are required to pledge at least one share as patrons of the Snowdrift.coop project.
* Team members are strongly encouraged to own more than one Snowdrift.coop share and to pledge to other projects funded through Snowdrift.coop, as well as to support cooperative businesses in their professional and personal capacities.

### Employee or director gifts

* Any professional or business-related gifts to cooperative employees or Directors with a value of $10 or more shall become property of the cooperative. It is the responsibility of employees to report gifts to the general manager.
* Gifts from the cooperative to individuals, either employees or persons outside the cooperative, may be given at the discretion of the general manager, but shall be $20 or less in value per gift.

### Employee training

* The cooperative encourages lifelong education through classroom, distance, and experiential learning, and will provide educational opportunities wherever feasible and relevant.
* Training necessary to job duties may be required as a condition of employment and will be identified, organized, and paid for by the cooperative.
* Employees may also suggest training they feel will further professional development. The cost of training programs that are particularly beneficial to both the cooperative and the employee may be paid by the cooperative.
* The cooperative will maintain a liberal leave policy for training that is primarily beneficial to the employee's personal development but will not reimburse training costs.

### Involuntary termination

* The cooperative will not involuntarily terminate an employee without just cause.
    * When a just cause is the fault of the employee, corrective action and progressive discipline shall always be preferred over immediate termination except in the most extreme cases.
    * Just causes for fault-based involuntary termination include, but are not limited to:
        * Blatant or malicious disregard for co-op policies
        * Insubordination or failure to follow applicable regulations
        * Excessive absenteeism
        * Using or being under the influence of intoxicants while on duty
        * Dishonesty
        * Misuse of co-op time or property
        * Acts that may harm or embarrass the cooperative
    * An employee involuntarily terminated with cause and fault shall forfeit all employee rights, shall not be eligible for rehire, and may not use the cooperative as a reference when seeking other employment.
    * Just cause termination may also be based on the business needs of the cooperative, without any fault on the part of the employee. 
    * A permanent employee terminated with cause but without fault is entitled to 4 weeks' notice or to 4 weeks' severance pay in lieu of notice.

### Voluntary termination

* Co-op employees who voluntarily terminate employment shall give 2 weeks written notice.
* Failure to give such notice may result in forfeiture of certain benefits, as determined by the general manager.

### Promotion

The cooperative has no set policy that provides for systematic or seniority promotion. However, management promises an unbiased and fair evaluation of an employee’s growth and value to the organization and will consider pay and responsibility advancement accordingly.

When a position becomes available, promotion should be made within the cooperative system whenever practical, recognizing that some positions requiring special skills, training, or experience may require considering external candidates.

### Probationary Period

* All new employees shall serve a six-month probationary period during which the cooperative and the employee shall have an opportunity to determine the employee's suitability for the position and vice versa.
* At the end of the six-month probation, the supervisor and the employee will meet together to determine if the job description and duties and the employee's performance are satisfactory to both parties.
* During the probationary period and the evaluation interview, both the employee and the supervisor have the right to initiate a "no-fault" termination, which does not render the employee ineligible for rehire or preclude them from using the cooperative as a reference when seeking other employment.
* If the employee remains with the cooperative after the probationary period, they will be classified as a regular full-time or regular part-time employee.

### Definition of Snowdrift.coop team membership

* All regular full-time and regular part-time employees are automatically official Snowdrift.coop team members, and Snowdrift-class cooperative members, with the attendant rights and restrictions.
* Employment agreements for contract workers shall specify whether they are entitled to Snowdrift-class membership for the duration of the contract. When determining whether or not to grant Snowdrift-class, the Board should consider:
    * Length of employment (longer contracts should be more likely to include membership)
    * Nature of work (the more central and essential to the cooperative mission, the more likely to include membership)
    * Contractee's current membership status (Project- or General-class members should move to Snowdrift-class during contract work)

## Education

The cooperative shall promote policies that support member education on the cooperative movement, FLO culture, and other topics relevant to the Mission. This shall include, but in no way be limited to:

* Sponsoring speakers and attendance at conferences
* Hosting educational blogs and web symposia
* Providing mentorship and internship opportunities within the organization.

## Committees

* The Board may charter committees and delegate rule-making authority to them within the scope of their charter.
* The Board may not delegate rulemaking on any matter requiring a general vote of the membership.
* The Board is the final arbiter and avenue of appeal for all decisions made by chartered committees.
* Committees must have a written charter which completely describes the committee's structure, membership policies, and meeting schedule.
    * The charter shall be put before the membership for review and discussion before it is voted on by the board of directors.
* Committees may be either ad-hoc (such as special election ballot and compensation proposal committees) or Standing Committees. Standing Committees currently authorized by the Board are:
    * Finance: Responsible for generating an annual budget for the Board to review, amend, and pass, and for ensuring adherence to accounting standards.
    * Technical: Handles day-to-day site operations and funding mechanism details. This committee may eventually be divided into IT and Business Operations Committees.
    * External Relations: Connects with the larger cooperative movement, promotes leadership and business connection, and engages with civic groups in furthering community interests.
    * Team Relations: Creates employment and volunteer policies, handles HR issues, and carries out performance evaluations.
    * Membership: Creates and enforces policies for member conduct and termination.
* All committees chartered by the Board shall use the same facilitated consensus and fallback decision-making process as the Board itself.
* Committee meetings should be open to the public unless they touch on private or sensitive information that requires discretion.
    * Private committee meetings must still generate and submit minutes, which may be made public after an appropriate interval has passed.
